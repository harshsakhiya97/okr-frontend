import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { UserAppRoutingModule } from './userapp-routing.module';
import { UtilityModule } from '../../../utility/src/app/utility.module';
import { UserManagementComponent } from './module/user/usermanagement.component';
import { UserService } from './module/user/service/user.service';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { CompanydetailsComponent } from './module/company/companydetails.component';
import { CompanyDetailService } from './module/company/service/companydetail.service';
import { ChangePasswordComponent } from './module/changepassword/changepassword.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { TabViewModule } from 'primeng/tabview';

@NgModule({
  declarations: [
    AppComponent,
    UserManagementComponent,
    CompanydetailsComponent,
    ChangePasswordComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    TabViewModule,
    UtilityModule,
    CKEditorModule,
    UserAppRoutingModule,
  ],
  providers: [UserService,CompanyDetailService],
  bootstrap: [AppComponent]
})
export class UserAppModule { }
