import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserManagementComponent } from './module/user/usermanagement.component';
import { CompanydetailsComponent } from './module/company/companydetails.component';
import { ChangePasswordComponent } from './module/changepassword/changepassword.component';

const routes: Routes = [
  {
    path: 'usermanagement',
    component: UserManagementComponent,
    data: {
      title: 'User Management',
      icon: 'icon-user',
     // caption: 'Management',
      status: true
    }
  },
  {
    path: 'companydetails',
    component: CompanydetailsComponent,
    data: {
      title: 'Company Details',
      icon: 'icon-user',
      caption: 'Management',
      status: true
    }
  },
  {
    path: 'companydetailsview',
    component: CompanydetailsComponent,
    data: {
      title: 'Company Details',
      icon: 'icon-user',
      caption: 'Management',
      status: true
    }
  },
  {
    path: 'changepassword',
    component: ChangePasswordComponent,
    data: {
      title: 'Change Password',
      icon: 'icon-user',
      caption: 'password',
      status: true
    }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserAppRoutingModule { }
