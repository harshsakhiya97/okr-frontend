import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthService } from 'projects/utility/src/app/module/service/auth.service';
import { CompanyDetailService } from './service/companydetail.service';
import { CompanyDetailMaster } from './model/companydetailsmaster.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
@Component({
  selector: 'app-companydetails',
  templateUrl: './companydetails.component.html',
  styleUrls: ['./companydetails.component.css']
})
export class CompanydetailsComponent implements OnInit {
  companyDetailMaster:CompanyDetailMaster = new CompanyDetailMaster();
  checkCompanyDetailView:boolean = false;
  public Editor = ClassicEditor;
  text: string;
  uploadedFiles: any={} ;
  public editor;
  public editorContent;
  public editorConfig = {
    placeholder: 'Put your things hear'
  };
  pluginsTest:any= [];

  constructor(private authService:AuthService,private companyDetailService:CompanyDetailService,
    private spinnerService:NgxSpinnerService,private router: Router) { }

  ngOnInit() {
    try{
      let test = Array.from( this.Editor.ui.componentFactory.names() );
      console.log("toolbar"+test);
      this.Editor.builtinPlugins.map( plugin => 
        this.pluginsTest.push(plugin.pluginName) 
        );

        console.log("pluginsTest"+this.pluginsTest)

    }catch(err){

    }

    if (this.router.url === '/panel/user/companydetailsview') {
      this.checkCompanyDetailView = true;
    }

    this.getCompanyDetail();
  }


  getCompanyDetail(): any {
    this.spinnerService.show();
  
    this.companyDetailService.getCompanyDetail(new CompanyDetailMaster()).subscribe(
      data => {
        this.companyDetailMaster = data;
        this.spinnerService.hide();
      },
      error => {
        if (error instanceof HttpErrorResponse) {
          if (error.status === 200) {
            let jsonString = error.error.text;
            jsonString = jsonString.substr(
              0,
              jsonString.indexOf('{"timestamp"')
            );
         //   this.spinnerService.hide();
            let data = JSON.parse(jsonString);
            this.companyDetailMaster = data;
          } else {
            this.authService.showMessage('error','Something went wrong', '');
          //  this.spinnerService.hide();
          }
          this.spinnerService.hide();
        }
      }
    );
  }

  saveCompanyDetail(): any {
    this.spinnerService.show();
  
    this.companyDetailService.saveCompanyDetail(this.companyDetailMaster,this.uploadedFiles[0]).subscribe(
      data => {
        this.companyDetailMaster = data;
        this.spinnerService.hide();
        this.getCompanyDetail();
        this.authService.addToast({title:'', msg:'Successfully Saved', showClose: true, 
        timeout: 5000, theme:'bootstrap', type:'success', position:'bottom-left', closeOther:true});
      },
      error => {
        if (error instanceof HttpErrorResponse) {
          if (error.status === 200) {
            let jsonString = error.error.text;
            jsonString = jsonString.substr(
              0,
              jsonString.indexOf('{"timestamp"')
            );
         //   this.spinnerService.hide();
            let data = JSON.parse(jsonString);
            this.companyDetailMaster = data;
            this.getCompanyDetail();
            this.authService.addToast({title:'', msg:'Successfully Saved', showClose: true, 
            timeout: 5000, theme:'bootstrap', type:'success', position:'bottom-left', closeOther:true});
          } else {
            this.authService.showMessage('error','Something went wrong', '');
          //  this.spinnerService.hide();
          }
          this.spinnerService.hide();
        }
      }
    );
  }

  myUploader(event) {
    console.log("myUploader");
    this.uploadedFiles = [];
    for(let file of event.files) {
            this.uploadedFiles.push(file);
        }
    }

}