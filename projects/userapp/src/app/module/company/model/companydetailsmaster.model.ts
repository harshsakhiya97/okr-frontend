export class CompanyDetailMaster {
    public companyDetailId: number;
    public name: string;
    public description: string;
    public email: string;
    public phone: string;
    public address: string;
    public facebookPage: string;
    public instagramPage: string;
    public twitterPage: string;
    public isDelete: string;
}
