import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { CompanyDetailMaster } from '../model/companydetailsmaster.model';

  
  @Injectable()
  export class CompanyDetailService {
  
    constructor(private http: HttpClient) { }

    saveCompanyDetail(companyDetailMaster:CompanyDetailMaster,files:any): Observable<any> {
      let formData:FormData = new FormData();
      //   formData.append('files', files);
      console.log("files",files)
         //for (let x = 0; x < files.length; x++) {
           formData.append("files",files );
        // };

         formData.append('companyDetailMaster', new Blob([JSON.stringify(companyDetailMaster)],
              {
                  type: "application/json"
              }));
      return this.http.post<any>(environment.apiUrl+'/companyDetail/saveCompanyDetail',formData);
    }

    getCompanyDetail(companyDetailMaster:CompanyDetailMaster): Observable<any> {
      return this.http.post<any>(environment.apiUrl+'/companyDetail/getCompanyDetail',companyDetailMaster);
    }

  }