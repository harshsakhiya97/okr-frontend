import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthService } from 'projects/utility/src/app/module/service/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../user/service/user.service';
import { UserLogin } from '../user/model/userlogin.model';
@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangePasswordComponent implements OnInit {
  user:UserLogin=new UserLogin();
  profileUser:UserLogin = new UserLogin();
  checkCurrentPassword: boolean = false;
  validationUserMsg: string = "";
  validationUserError: boolean = false;
  constructor(private authService:AuthService,private userService:UserService,private spinnerService:NgxSpinnerService) { }

  ngOnInit() {
    
    this.user=JSON.parse(localStorage.getItem("userSession"));
    this.profileUser=JSON.parse(localStorage.getItem("userSession"));
  //  this.getUserByEmailOrPhoneApi();
  }

  resetForm(){
    this.checkCurrentPassword = false;
    this.validationUserError = false;
    this.validationUserMsg = "";
    this.profileUser = new UserLogin();

  }

  changePassword(): any {
    this.spinnerService.show();
    this.validationUserError = this.validateUserForm();

    if(this.validationUserError){
      this.spinnerService.hide();
      return;
    }

    this.userService.changePassword(this.profileUser).subscribe(
      data => { 
        let statusData = data;
        this.spinnerService.hide();
      //  this.getUserByEmailOrPhoneApi();
        if(statusData=="SUCCESS"){
          this.resetForm();
          this.authService.addToast({title:'', msg:'Successfully Updated', showClose: true, 
          timeout: 5000, theme:'bootstrap', type:'success', position:'bottom-left', closeOther:true});
        }else{
          this.authService.addToast({title:'', msg:'Current Password Not Matching', showClose: true, 
          timeout: 5000, theme:'bootstrap', type:'error', position:'bottom-left', closeOther:true});
        }
        
      },
      error => {
        if (error instanceof HttpErrorResponse) {
          if (error.status === 200) {
            let statusData = error.error.text;
          
         //   this.spinnerService.hide();
          //  let data = JSON.parse(jsonString);
       //     this.loginMaster = data;
        //    this.getUserByEmailOrPhoneApi();

        if(statusData=="SUCCESS"){
          this.resetForm();
          this.checkCurrentPassword = false;
          this.authService.addToast({title:'', msg:'Successfully Updated', showClose: true, 
          timeout: 5000, theme:'bootstrap', type:'success', position:'bottom-left', closeOther:true});
        }else{
          this.authService.addToast({title:'', msg:'Current Password Not Matching', showClose: true, 
          timeout: 5000, theme:'bootstrap', type:'error', position:'bottom-left', closeOther:true});
        }

           
          } else {
            this.authService.showMessage('error','Something went wrong', '');
          //  this.spinnerService.hide();
          }
          this.spinnerService.hide();
        }
      }
    );
  }



  validateUserForm(){
    this.validationUserMsg = "";
    this.validationUserError = false;
    let validationStr="";
    let validateCheck = false;

    if(this.profileUser.newPassword==null || this.profileUser.newPassword==undefined || 
      this.profileUser.newPassword==""){
        validateCheck = true;
        validationStr=validationStr+"New Password mandatory, ";     
    }

    if(this.profileUser.currentPassword==null || this.profileUser.currentPassword==undefined || 
      this.profileUser.currentPassword==""){
        validateCheck = true;
        validationStr=validationStr+"Current Password mandatory, ";     
    }

    if(this.profileUser.confirmPassword==null || this.profileUser.confirmPassword==undefined || 
      this.profileUser.confirmPassword==""){
        validateCheck = true;
        validationStr=validationStr+"Confirm Password mandatory, ";     
    }
    console.log("validateUserForm");
    if(this.profileUser.newPassword!=null && this.profileUser.newPassword!=undefined && 
      this.profileUser.newPassword!="" && this.profileUser.confirmPassword!=null && this.profileUser.confirmPassword!=undefined && 
      this.profileUser.confirmPassword!=""  && this.profileUser.newPassword!=this.profileUser.confirmPassword){
        validateCheck = true;
        validationStr=validationStr+"New and Confirm Password not Matching, ";     
    }

    if(validateCheck){  
      validationStr = validationStr.replace(/,(?=[^,]*$)/, '');
      this.validationUserMsg = validationStr;
      this.validationUserError = true;
    }
    
    return validateCheck;

  }

}
