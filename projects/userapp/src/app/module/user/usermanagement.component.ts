import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { CardComponent } from 'src/app/shared/card/card.component';
import { AuthService } from 'projects/utility/src/app/module/service/auth.service';
import { FilterModuleMaster } from 'projects/utility/src/app/module/common/filter/model/filtermodulemaster.model';
import { ModuleMaster } from 'projects/utility/src/app/module/system/model/modulemaster.model';
import { FieldConfigurationService } from 'projects/utility/src/app/module/field/service/fieldconfiguration.service';
import { UserService } from './service/user.service';
import swal from 'sweetalert2';
import { UserLogin } from './model/userlogin.model';
import { UserLoginLogs } from './model/userLoginLogs.model';
import { DownloadItem } from 'projects/okr/src/app/module/okr-management/model/downloadItem.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { DownloadItemService } from 'projects/okr/src/app/module/okr-management/service/downloadItem.service';
import { UserLoginLogsService } from './service/userLoginLogs.service';

@Component({
  selector: 'app-usermanagement',
  templateUrl: './usermanagement.component.html',
  styleUrls: ['./usermanagement.component.scss']
})
export class UserManagementComponent implements OnInit {
  @ViewChild('card') cardComponent:CardComponent;
  @ViewChild('modalDownloadLogs') modalDownloadLogs:any;
  @ViewChild('modalLoginLogs') modalLoginLogs:any;

  user:UserLogin=new UserLogin();
  userFilter:UserLogin=new UserLogin();
  userLogin:UserLogin=new UserLogin();

  totalRecords:number=0;
  pageSize:number=10;
  pageNumber:number=0;
  tableSettings:any={};
  tableCols:any[]=[];
  userList:UserLogin[]=[];
  actionSettings:any={};

  advanceCols:any[]=[];
  quickCols:any[]=[];
  filterModuleMasterList:FilterModuleMaster[]=[];

  tableView:boolean = true;

  userLoginLogsFilter: UserLoginLogs = new UserLoginLogs();
  userLoginLogsTableSettings:any={};
  userLoginLogsTableCols:any[]=[];
  userLoginLogsList: UserLoginLogs[]=[];
  userLoginLogsTotalRecords: number=0;
  userLoginLogsPageSize: number=10;
  userLoginLogsPageNumber: number=0;

  downloadItemFilter: DownloadItem = new DownloadItem();
  downloadItemTableSettings:any={};
  downloadItemTableCols:any[]=[];
  downloadItemList: DownloadItem[]=[];
  downloadItemTotalRecords: number=0;
  downloadItemPageSize: number=10;
  downloadItemPageNumber: number=0;

  constructor(private userService: UserService,private authService:AuthService,
    private fieldConfigurationService:FieldConfigurationService,
    private spinnerService: NgxSpinnerService, private downloadItemService: DownloadItemService,
    private userLoginLogsService: UserLoginLogsService) { }

  ngOnInit() {
    this.user = this.authService.getUserSession();
    this.setTableSettings();
    this.getUserList();
    this.getFilterListByModuleAndType("QUICK");
  }

  setTableSettings(){
    this.actionSettings = {
      download: true,
      signIn: true,
      isDeactive: true
    };

    this.tableCols=[
      
      {filterfield:'action',hidden:true,field:'action', header:'Action',width : '60px',fieldType:'na',columnType:'action',actionSettings:this.actionSettings}  ,
      {filterfield:'uniqueCustomerId',field:'uniqueCustomerId',header:'Unique Id',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'fullName',field:'fullName',header:'Name',width : '150px',fieldType:'text',columnType:'data'},
      {filterfield:'phone',field:'phone',header:'Phone',width : '150px',fieldType:'text',columnType:'data'},
      {filterfield:'email',field:'email',header:'Email',width : '150px',fieldType:'text',columnType:'data'},
      {filterfield:'companyName',field:'companyName',header:'Company Name',width : '150px',fieldType:'text',columnType:'data'},
      {filterfield:'location',field:'location',header:'Location',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'totalDownloads',field:'totalDownloads',header:'Total Downloads',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'industry.name',field:'industry',subfield:'name',header:'Industry',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'designation.name',field:'designation',subfield:'name',header:'Designation',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'designationLevel.name',field:'designationLevel',subfield:'name',header:'Designation Level',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'department.name',field:'department',subfield:'name',header:'Department',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'region.name',field:'region',subfield:'name',header:'Region',width : '120px',fieldType:'text',columnType:'data'},
        
    ];
    
    this.tableSettings = {
      autoLayout: true,
      sortMode: "multiple",
      scrollable: true,
      width: "100%",
      scrollHeight: "500px",
      reorderableColumns: true,
      resizableColumns: true,
      tableFilter:false,
      paginate:true
    };

    this.userLoginLogsTableCols=[ 
      {filterfield:'createdDate',field:'createdDate',header:'Login Date',width : '70px',fieldType:'date',columnType:'data',dateFormat:'medium'},
    ];

    this.userLoginLogsTableSettings = {
      autoLayout: true,
      sortMode: "multiple",
      scrollable: true,
      width: "350%",
      scrollHeight: "500px",
      reorderableColumns: true,
      resizableColumns: true,
      tableFilter:false,
      paginate:true
    };


    this.downloadItemTableCols=[ 
      {filterfield:'createdDate',field:'createdDate',header:'Download Date',width : '150px',fieldType:'date',columnType:'data',dateFormat:'medium'},
      {filterfield:'okrMaster.objective',field:'okrMaster',subfield:'objective',header:'OKR Objective',width : '150px',fieldType:'text',columnType:'data'},
    ];

    this.downloadItemTableSettings = {
      autoLayout: true,
      sortMode: "multiple",
      scrollable: true,
      width: "350%",
      scrollHeight: "500px",
      reorderableColumns: true,
      resizableColumns: true,
      tableFilter:false,
      paginate:true
    };


  }

  toggleView(){
    this.tableView = !this.tableView
    if(this.tableView){
      this.getUserList();
    }
  }

  getUserList(){
    this.getUserListLength();
    this.userService.getUserList(this.userFilter).subscribe( data => {
    this.userList=data;
    // this.spinnerService.hide();
    this.cardComponent.hideLoader();
    },error =>{
      if (error instanceof HttpErrorResponse) {
  
        if (error.status === 200) {
          // console.log(error.error.text);
          //json circular break at server side always add
          //extra information of timestamp and status along with
          //main json so parsing occurs still at status 200
          let jsonString=error.error.text;
          jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
          this.userList=JSON.parse(jsonString);
        //   this.spinnerService.hide();
        }else{
      //  this.authService.showMessage('error','Something Went Wrong','');
      //  this.spinnerService.hide();
      }
      this.cardComponent.hideLoader();
      }
    });
  }
   
  getUserListLength(){
    this.userService.getUserListLength(this.userFilter).subscribe( data => {
    this.totalRecords=data;
    },error =>{
      if (error instanceof HttpErrorResponse) {
  
        if (error.status === 200) {
          // console.log(error.error.text);
          //json circular break at server side always add
          //extra information of timestamp and status along with
          //main json so parsing occurs still at status 200
          let jsonString=error.error.text;
          jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
          this.totalRecords=JSON.parse(jsonString);
        }else{
      //   this.authService.showMessage('error','Something Went Wrong','');
      }
  
      }
    });
  }

  paginate(event) {
    this.pageSize=event.rows;
    this.userFilter.pageNumber=event.page;
    this.userFilter.pageSize=this.pageSize;
    this.getUserList();
  }

  getFilterListByModuleAndType(type:string){
    let filterModuleMaster=new FilterModuleMaster();
    filterModuleMaster.moduleMaster=new ModuleMaster();
    filterModuleMaster.moduleMaster.moduleCode="USER";

    if(type==="ADVANCE"){
      filterModuleMaster.filterType="ADVANCE";
    }
    if(type==="QUICK"){
      filterModuleMaster.filterType="QUICK";
    }

    this.fieldConfigurationService.getFilterListByModuleAndType(filterModuleMaster).subscribe( data => {

      console.log('fieldConfigurationService');
      console.log(data);

      if(type==="ADVANCE"){
        this.filterModuleMasterList=data;

      }
      if(type==="QUICK"){
        console.log("Quick filter called");
        for(let filterModuleMaster of data){

            if(filterModuleMaster.filterLookupList!=null && filterModuleMaster.filterLookupList!==undefined &&
              filterModuleMaster.filterLookupList.length > 0){
                  for(let data of filterModuleMaster.filterLookupList ){
                      console.log(data);
                  }
            }


            this.quickCols.push( {
              filterModuleId:filterModuleMaster.filterModuleId,
              fieldName: filterModuleMaster.fieldName,
              fieldType:filterModuleMaster.fieldType,
              width : filterModuleMaster.width, 
              lookupValues:filterModuleMaster.lookupValues,
              lookupSelect:filterModuleMaster.lookupSelect,
              singleSelect:filterModuleMaster.singleSelect,
              optionLabel:filterModuleMaster.lookupLabelValue,
              defaultLabel:filterModuleMaster.defaultLabel,
              filterModuleMaster:filterModuleMaster,
              filterLookupList:filterModuleMaster.filterLookupList,
              lookupLabelValue:filterModuleMaster.lookupLabelValue

          });
          }
      }


    },error =>{
      if (error instanceof HttpErrorResponse) {
        console.log("getFilterListByModuleAndType error");
        if (error.status === 200) {

            let jsonString=error.error.text;
            jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
            this.filterModuleMasterList=JSON.parse(jsonString);
        }else{
          this.authService.showMessage('error','Something went wrong', '');
      }

      }
    });
  }

  filterPerform(){
    this.getUserList();
  }

  activeDeactive(user:UserLogin){
    // this.cardComponent.showLoader();
    this.userService.activeDeactiveUser(user).subscribe( data => {
    // this.okrMasterList=data;
    // this.spinnerService.hide();
    this.cardComponent.hideLoader();

    if(user.isDeactive=='Y'){
      this.authService.addToast({title:'', msg:'Successfully Deactivated', showClose: true, 
      timeout: 5000, theme:'bootstrap', type:'wait', position:'bottom-left', closeOther:true});
    }else  if(user.isDeactive=='N'){
      this.authService.addToast({title:'', msg:'Successfully Activated', showClose: true, 
      timeout: 5000, theme:'bootstrap', type:'wait', position:'bottom-left', closeOther:true});
    }


    this.cardComponent.hideLoader();
    },error =>{
      if (error instanceof HttpErrorResponse) {
  
        if (error.status === 200) {
            // console.log(error.error.text);
            //json circular break at server side always add
            //extra information of timestamp and status along with
            //main json so parsing occurs still at status 200
            let jsonString=error.error.text;
            jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
          this.cardComponent.hideLoader();
            if(user.isDeactive=='Y'){
              this.authService.addToast({title:'', msg:'Successfully Deactivated', showClose: true, 
              timeout: 5000, theme:'bootstrap', type:'wait', position:'bottom-left', closeOther:true});
            }else  if(user.isDeactive=='N'){
              this.authService.addToast({title:'', msg:'Successfully Activated', showClose: true, 
              timeout: 5000, theme:'bootstrap', type:'wait', position:'bottom-left', closeOther:true});
            }

          // this.okrMasterList=JSON.parse(jsonString);
        //   this.spinnerService.hide();
        }else{
          this.cardComponent.hideLoader();
        //  this.authService.showMessage('error','Something Went Wrong','');
        //  this.spinnerService.hide();
      }
      this.cardComponent.hideLoader();
      }
    });
  }

  activeDeactiveConfirmation(user:UserLogin) {
    let message="";
    if(user.isDeactive=='Y'){
      message  = "Deactive";
    }else{
      message  = "Active";
    }

    swal({
      title: 'Are you sure '+message+' ?',
      text: 'You wont be able to revert',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#efde26',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, '+message+' it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success m-r-10',
      cancelButtonClass: 'btn btn-primary',
      buttonsStyling: false
    }).then( (reponse)=>{ 
      console.log("dismiss "+reponse);

      if (reponse.dismiss!=null && reponse.dismiss!=undefined) {
        swal(
          'Cancelled',
          '',
          'error'
        );

        if(user.isDeactive=='Y'){
          user.isDeactive='N';
        }else{
          user.isDeactive='Y';
        }

        
      }else{
        this.activeDeactive(user);
        swal(
          message+' !',
          'You Have Successfully '+message+".",
          'success'
        );
      }
      
    }).catch(swal.noop);
  }

  resetFilter(){
    this.userFilter = new UserLogin();
    this.getUserList();
  }

  viewDownloadLogs(userLogin: UserLogin){
    this.userLogin = userLogin;
    this.modalDownloadLogs.show();
    this.getDownloadItemList();
  }

  getDownloadItemList(){
  //  this.cardComponent.showLoader();
    this.spinnerService.show();

    let userLoginTemp = new UserLogin();
    userLoginTemp.id = this.userLogin.id;
    this.downloadItemFilter.user = userLoginTemp;
    this.downloadItemFilter.pageNumber=(this.downloadItemFilter.pageNumber == null) ? this.downloadItemPageNumber : this.downloadItemFilter.pageNumber ;
    this.downloadItemFilter.pageSize=(this.downloadItemFilter.pageSize == null) ? this.downloadItemPageSize : this.downloadItemFilter.pageSize ;

    this.getDownloadItemListLength();
    this.downloadItemService.getDownloadItemList(this.downloadItemFilter).subscribe( data => {

      this.downloadItemList=data;
      this.spinnerService.hide();
  //     this.cardComponent.hideLoader();
    },error =>{
      if (error instanceof HttpErrorResponse) {

        if (error.status === 200) {
          // console.log(error.error.text);
          //json circular break at server side always add
          //extra information of timestamp and status along with
          //main json so parsing occurs still at status 200
          let jsonString=error.error.text;
          jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
          this.downloadItemList=JSON.parse(jsonString);
          this.spinnerService.hide();
        }else{
      //  this.authService.showMessage('error','Something Went Wrong','');
      //  this.spinnerService.hide();
      }
      this.spinnerService.hide();
    // this.cardComponent.hideLoader();
      }
    });
  }

  getDownloadItemListLength(){
  this.downloadItemService.getDownloadItemListLength(this.downloadItemFilter).subscribe( data => {
  this.downloadItemTotalRecords=data;
  },error =>{
    if (error instanceof HttpErrorResponse) {

      if (error.status === 200) {
        // console.log(error.error.text);
        //json circular break at server side always add
        //extra information of timestamp and status along with
        //main json so parsing occurs still at status 200
        let jsonString=error.error.text;
        jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
        this.downloadItemTotalRecords=JSON.parse(jsonString);
      }else{
    //   this.authService.showMessage('error','Something Went Wrong','');
    }

    }
  });
  }

  downloadItemPaginate(event) {
  this.downloadItemPageSize=event.rows;
  this.downloadItemFilter.pageNumber=event.page;
  this.downloadItemFilter.pageSize=this.downloadItemPageSize;
  this.getDownloadItemList();
  }

  viewLoginLogs(userLogin: UserLogin){
    this.userLogin = userLogin;
    this.modalLoginLogs.show();
    this.getUserLoginLogsList();
  }

  getUserLoginLogsList(){
  //  this.cardComponent.showLoader();
    this.spinnerService.show();
    let userLoginTemp = new UserLogin();
    userLoginTemp.id = this.userLogin.id;
    this.userLoginLogsFilter.user = userLoginTemp;
    this.userLoginLogsFilter.pageNumber=(this.userLoginLogsFilter.pageNumber == null) ? this.downloadItemPageNumber : this.userLoginLogsFilter.pageNumber ;
    this.userLoginLogsFilter.pageSize=(this.userLoginLogsFilter.pageSize == null) ? this.downloadItemPageSize : this.userLoginLogsFilter.pageSize ;

    this.getUserLoginLogsListLength();
    this.userLoginLogsService.getUserLogsList(this.userLoginLogsFilter).subscribe( data => {

      this.userLoginLogsList=data;
      this.spinnerService.hide();
  //     this.cardComponent.hideLoader();
    },error =>{
      if (error instanceof HttpErrorResponse) {

        if (error.status === 200) {
          // console.log(error.error.text);
          //json circular break at server side always add
          //extra information of timestamp and status along with
          //main json so parsing occurs still at status 200
          let jsonString=error.error.text;
          jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
          this.userLoginLogsList=JSON.parse(jsonString);
          this.spinnerService.hide();
        }else{
      //  this.authService.showMessage('error','Something Went Wrong','');
      //  this.spinnerService.hide();
      }
      this.spinnerService.hide();
    // this.cardComponent.hideLoader();
      }
    });
  }

  getUserLoginLogsListLength(){
  this.userLoginLogsService.getUserLogsLength(this.userLoginLogsFilter).subscribe( data => {
  this.downloadItemTotalRecords=data;
  },error =>{
    if (error instanceof HttpErrorResponse) {

      if (error.status === 200) {
        // console.log(error.error.text);
        //json circular break at server side always add
        //extra information of timestamp and status along with
        //main json so parsing occurs still at status 200
        let jsonString=error.error.text;
        jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
        this.downloadItemTotalRecords=JSON.parse(jsonString);
      }else{
    //   this.authService.showMessage('error','Something Went Wrong','');
    }

    }
  });
  }

  userLoginLogsPaginate(event) {
  this.userLoginLogsPageSize=event.rows;
  this.userLoginLogsFilter.pageNumber=event.page;
  this.userLoginLogsFilter.pageSize=this.userLoginLogsPageSize;
  this.getUserLoginLogsList();
  }


}
