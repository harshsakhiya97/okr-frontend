import { UserLogin } from 'projects/userapp/src/app/module/user/model/userlogin.model';

export class UserLoginLogs{
    public userLoginLogsId: number;
	public user: UserLogin;
    public userId: number;
	public isDelete: String;
	public pageSize: number;
	public pageNumber: number;
	public createdDate:Date;
}