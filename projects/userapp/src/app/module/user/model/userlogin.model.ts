import { Role } from './role.model';
import { User } from './user';
import { AccessControlMaster } from 'projects/utility/src/app/module/system/model/accesscontrolmaster.model';
import { PredefinedMaster } from 'src/app/shared/predefinedsettings/model/predefinedmaster.model';

export class UserLogin {
    public id:number
    public uniqueCustomerId:string;
    public firstName: string;
    public middleName: string;
    public lastName: string;
    public email: string;
    public phone: string;
    public username:string;
    public password:string;
    public userProfilePath:string;
    public fileName:string;
    public industry:PredefinedMaster;
    public industryId:number;
    public designation:PredefinedMaster;
    public designationId:number;
    public designationLevel:PredefinedMaster;
    public designationLevelId:number;
    public department:PredefinedMaster;
    public departmentId:number;
    public region:PredefinedMaster;
    public regionId:number;
    public companyName:string;
    public location:string;
    public companyWebsite:string;
    public totalDownloads:number;
    public isDelete: string;
    public isDeactive: string;
    public currentOTP: string;
    public roleName:string;
    public confirmPassword:string;
    public currentPassword:string;
    public newPassword:string;
    public confirmNewPassword:string;
    public title:string;
    public message:string;
    public searchUser:string;
    public role:Role;
    public roles:Role[];
    public accessControlMasterList:AccessControlMaster[];
    public pageNumber:number;
    public pageSize:number;

    public getName() {
        return this.firstName + ' ' +this.middleName +' ' + this.lastName;
    }

}
