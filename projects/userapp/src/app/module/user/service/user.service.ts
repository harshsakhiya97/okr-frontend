import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserLogin } from '../model/userlogin.model';
  
  @Injectable()
  export class UserService {
  
    constructor(private http: HttpClient) { }

    getUserDetailsByUsername(user:UserLogin): Observable<any> {
      return this.http.post<any>(environment.apiUrl+'/user/getUserDetailsByUsername',user);
  }

    getUserList(user:UserLogin): Observable<any> {
        return this.http.post<any>(environment.apiUrl+'/user/getUserList',user);
    }

    getUserListLength(user:UserLogin): Observable<any> {
      return this.http.post<any>(environment.apiUrl+'/user/getUserListLength',user);
    }

    activeDeactiveUser(user:UserLogin): Observable<any> {
      return this.http.post<any>(environment.apiUrl+'/user/activeDeactiveUser',user);
    }

    getPostRequestList(user:UserLogin): Observable<any> {
      return this.http.post<any>(environment.apiUrl+'/user/getPostRequestList',user);
    }

    getPostRequestListLength(user:UserLogin): Observable<any> {
      return this.http.post<any>(environment.apiUrl+'/user/getPostRequestListLength',user);
    }

    updatePostPermission(user:UserLogin): Observable<any> {
      return this.http.post<any>(environment.apiUrl+'/user/updatePostPermission',user);
    }

    sendOtpToUser(user:UserLogin): Observable<any> {
      return this.http.post<any>(environment.apiUrl+'/user/sendOtpToUser',user);
    }

    changePassword(user:UserLogin): Observable<any> {
      return this.http.put<any>(environment.apiUrl+'/user/changePassword',user);
    }

    
    forgetPassword(user:UserLogin): Observable<any> {
      return this.http.post<any>(environment.apiUrl+'/user/forgetPassword',user);
    }


  }