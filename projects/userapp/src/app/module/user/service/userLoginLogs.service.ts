import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserLogin } from '../model/userlogin.model';
import { UserLoginLogs } from '../model/userLoginLogs.model';

@Injectable()
export class UserLoginLogsService {

    constructor(private http: HttpClient) { }

    getUserLogsList(userLoginLogs:UserLoginLogs): Observable<any> {
        return this.http.post<any>(environment.apiUrl+'/userlog/getUserLogsList',userLoginLogs);
    }

    getUserLogsLength(userLoginLogs:UserLoginLogs): Observable<any> {
      return this.http.post<any>(environment.apiUrl+'/userlog/getUserLogsLength',userLoginLogs);
    }

}