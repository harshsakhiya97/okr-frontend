import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PredefinedsettingsComponent } from 'src/app/shared/predefinedsettings/predefinedsettings.component';
import { AdminDashboardComponent } from './module/admin-dashboard/admin-dashboard.component';
import { OkrManagementComponent } from './module/okr-management/okr-management.component';


const routes: Routes = [
  {
    path: 'dashboard',
    component: AdminDashboardComponent,
    data: {
      title: 'Dashboard',
      icon: 'icon-user',
     // caption: 'Management',
      status: true
    }
  },
  {
    path: 'okr-master',
    component: OkrManagementComponent,
    data: {
      title: 'OKR Master',
      icon: 'icon-user',
     // caption: 'Management',
      status: true
    }
  },
  {
    path: 'other-masters',
    component: PredefinedsettingsComponent,
    data: {
      title: 'Other Masters',
      icon: 'icon-user',
     // caption: 'Management',
      status: true
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OkrRoutingModule { }
