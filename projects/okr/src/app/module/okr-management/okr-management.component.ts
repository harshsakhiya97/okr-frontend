import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgxCsvParser, NgxCSVParserError } from 'ngx-csv-parser';
import { NgxSpinnerService } from 'ngx-spinner';
import { ConfirmationService } from 'primeng/api';
import { OkrMaster } from 'projects/okr/src/app/module/okr-management/model/okrmaster.model';
import { OkrService } from 'projects/okr/src/app/module/okr-management/service/okr.service';
import { UserLogin } from 'projects/userapp/src/app/module/user/model/userlogin.model';
import { FilterModuleMaster } from 'projects/utility/src/app/module/common/filter/model/filtermodulemaster.model';
import { FieldConfigurationService } from 'projects/utility/src/app/module/field/service/fieldconfiguration.service';
import { AuthService } from 'projects/utility/src/app/module/service/auth.service';
import { ModuleMaster } from 'projects/utility/src/app/module/system/model/modulemaster.model';
import { CardComponent } from 'src/app/shared/card/card.component';
import { PredefinedMaster } from 'src/app/shared/predefinedsettings/model/predefinedmaster.model';
import { PredefinedService } from 'src/app/shared/predefinedsettings/service/predefined.service';
import { environment } from 'src/environments/environment';
import swal from 'sweetalert2';
import { DownloadItem } from './model/downloadItem.model';
import { DownloadItemService } from './service/downloadItem.service';

declare var require: any
 const FileSaver = require('file-saver');

@Component({
  selector: 'app-okr-management',
  templateUrl: './okr-management.component.html',
  styleUrls: ['./okr-management.component.css']
})

export class OkrManagementComponent implements OnInit {
  @ViewChild('card') cardComponent:CardComponent;
  @ViewChild('okrCsvFile') okrCsvFile:any;
  @ViewChild('okrCsvFileImport') okrCsvFileImport:any;
  @ViewChild('modalDownloadLogs') modalDownloadLogs:any;
  @ViewChild('modalOkrImport') modalOkrImport:any;

  userLogin: UserLogin = new UserLogin();
  okrMaster: OkrMaster=new OkrMaster();
  okrMasterFilter: OkrMaster=new OkrMaster();
  okrMasterList: OkrMaster[]=[];
  totalRecords: number=0;
  pageSize: number=10;
  pageNumber: number=0;
  tableView: Boolean = true;

  tableSettings:any={};
  tableCols:any[]=[];
  advanceCols:any[]=[];
  quickCols:any[]=[];
  filterModuleMasterList:FilterModuleMaster[]=[];

  addCheck:boolean = true;
  uploadedFiles:any[]=[];

  industryList: PredefinedMaster[]=[];
  designationList: PredefinedMaster[]=[];
  designationLevelList: PredefinedMaster[]=[];
  departmentList: PredefinedMaster[]=[];
  subDepartmentList: PredefinedMaster[]=[];

  downloadItemFilter: DownloadItem = new DownloadItem();
  downloadItemTableSettings:any={};
  downloadItemTableCols:any[]=[];
  downloadItemList: DownloadItem[]=[];
  downloadItemTotalRecords: number=0;
  downloadItemPageSize: number=10;
  downloadItemPageNumber: number=0;

  importOkrMasterList: OkrMaster[]=[];
  csvRecords: any[] = [];
  header = false;

  constructor(private okrService: OkrService,
    private downloadItemService: DownloadItemService,
    private authService:AuthService,
    private confirmationService: ConfirmationService,
    private fieldConfigurationService:FieldConfigurationService,private router: Router,
    private spinnerService:NgxSpinnerService,
    private predefinedService:PredefinedService,
    private ngxCsvParser: NgxCsvParser) { }

  ngOnInit() {
    this.userLogin = this.authService.getUserSession();

    this.setTableSettings();
    this.getOkrMasterList();
    this.getFilterListByModuleAndType();

    this.getPredefinedListByType("INDUSTRY");
    this.getPredefinedListByType("DESIGNATION");
    this.getPredefinedListByType("DESIGNATION-LEVEL");
    this.getPredefinedListByType("DEPARTMENT");
    this.getPredefinedListByType("SUB-DEPARTMENT");
    
  }
  
  toggleView(){
    this.tableView = !this.tableView
    if(this.tableView){
      this.getOkrMasterList() 
    }
  }

  setTableSettings(){
    let actionSettings = {
      edit: true,
      delete: true,
      download: true,
    };

    this.tableCols=[ 
      {filterfield:'action',field:'action',hidden:true, header:'Action',width : '60px',fieldType:'na',columnType:'action',actionSettings:actionSettings},  
      {filterfield:'objective',field:'objective',header:'Objective',width : '200px',fieldType:'text',columnType:'data'},
      {filterfield:'keyResult',field:'keyResult',header:'Key Result',width : '250px',fieldType:'text',columnType:'data'},
      {filterfield:'totalDownloads',field:'totalDownloads',header:'Total Downloads',width : '100px',fieldType:'text',columnType:'data'},
      {filterfield:'totalAccessed',field:'totalAccessed',header:'Total Accessed',width : '100px',fieldType:'text',columnType:'data'},
      {filterfield:'industry.name',field:'industry',subfield:'name',header:'Industry',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'designation.name',field:'designation',subfield:'name',header:'Designation',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'designationLevel.name',field:'designationLevel',subfield:'name',header:'Designation Level',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'department.name',field:'department',subfield:'name',header:'Department',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'subDepartment.name',field:'subDepartment',subfield:'name',header:'Sub Department',width : '120px',fieldType:'text',columnType:'data'},
    ];
        
    this.tableSettings = {
      autoLayout: true,
      sortMode: "multiple",
      scrollable: true,
      width: "100%",
      scrollHeight: "500px",
      reorderableColumns: true,
      resizableColumns: true,
      tableFilter:false,
      paginate:true
    };

    this.downloadItemTableCols=[ 
      {filterfield:'createdDate',field:'createdDate',header:'Download Date',width : '70px',fieldType:'date',columnType:'data',dateFormat:'medium'},
      {filterfield:'user.fullName',field:'user',subfield:'fullName',header:'User',width : '70px',fieldType:'text',columnType:'data'},
    ];

    this.downloadItemTableSettings = {
      autoLayout: true,
      sortMode: "multiple",
      scrollable: true,
      width: "350%",
      scrollHeight: "500px",
      reorderableColumns: true,
      resizableColumns: true,
      tableFilter:false,
      paginate:true
    };


  }

  getOkrMasterList(){
  //  this.cardComponent.showLoader();
      this.spinnerService.show();
      this.getOkrMasterListLength();

      this.okrMasterFilter.pageNumber=(this.okrMasterFilter.pageNumber == null) ? this.pageNumber : this.okrMasterFilter.pageNumber ;
      this.okrMasterFilter.pageSize=(this.okrMasterFilter.pageSize == null) ? this.pageSize : this.okrMasterFilter.pageSize ;

      this.okrService.getOkrMasterList(this.okrMasterFilter).subscribe( data => {
        console.log(data);
      this.okrMasterList=data;
      this.spinnerService.hide();
  //     this.cardComponent.hideLoader();
      },error =>{
        if (error instanceof HttpErrorResponse) {
    
          if (error.status === 200) {
            // console.log(error.error.text);
            //json circular break at server side always add
            //extra information of timestamp and status along with
            //main json so parsing occurs still at status 200
            let jsonString=error.error.text;
            jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
            this.okrMasterList=JSON.parse(jsonString);
            this.spinnerService.hide();
          }else{
        //  this.authService.showMessage('error','Something Went Wrong','');
        //  this.spinnerService.hide();
        }
        this.spinnerService.hide();
      // this.cardComponent.hideLoader();
        }
      });
  }
   
  getOkrMasterListLength(){
    this.okrService.getOkrMasterListLength(this.okrMasterFilter).subscribe( data => {
    this.totalRecords=data;
    },error =>{
      if (error instanceof HttpErrorResponse) {
  
        if (error.status === 200) {
          // console.log(error.error.text);
          //json circular break at server side always add
          //extra information of timestamp and status along with
          //main json so parsing occurs still at status 200
          let jsonString=error.error.text;
          jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
          this.totalRecords=JSON.parse(jsonString);
        }else{
      //   this.authService.showMessage('error','Something Went Wrong','');
      }
  
      }
    });
  }

  paginate(event) {
    this.pageSize=event.rows;

    this.okrMasterFilter.pageNumber=event.page;
    this.okrMasterFilter.pageSize=this.pageSize;
    this.getOkrMasterList();
  }

  getFilterListByModuleAndType(){
    let filterModuleMaster=new FilterModuleMaster();
    filterModuleMaster.moduleMaster=new ModuleMaster();
    filterModuleMaster.moduleMaster.moduleCode="OKR-MGNT";

    let type = "QUICK";

    if(type==="ADVANCE"){
      filterModuleMaster.filterType="ADVANCE";
    }
    if(type==="QUICK"){
      filterModuleMaster.filterType="QUICK";
    }

    this.fieldConfigurationService.getFilterListByModuleAndType(filterModuleMaster).subscribe( data => {

      if(type==="ADVANCE"){
        this.filterModuleMasterList=data;
    
      }
      if(type==="QUICK"){
        console.log("Quick filter called");
        for(let filterModuleMaster of data){

            if(filterModuleMaster.filterLookupList!=null && filterModuleMaster.filterLookupList!==undefined &&
              filterModuleMaster.filterLookupList.length > 0){
                  for(let data of filterModuleMaster.filterLookupList ){
                      console.log(data);
                  }
            }


              this.quickCols.push( {
                filterModuleId:filterModuleMaster.filterModuleId,
                fieldName: filterModuleMaster.fieldName,
                fieldType:filterModuleMaster.fieldType,
                width : filterModuleMaster.width, 
                lookupValues:filterModuleMaster.lookupValues,
                lookupSelect:filterModuleMaster.lookupSelect,
                singleSelect:filterModuleMaster.singleSelect,
                optionLabel:filterModuleMaster.lookupLabelValue,
                defaultLabel:filterModuleMaster.defaultLabel,
                filterModuleMaster:filterModuleMaster,
                filterLookupList:filterModuleMaster.filterLookupList,
                lookupLabelValue:filterModuleMaster.lookupLabelValue

            });
        }
      }


    },error =>{
      if (error instanceof HttpErrorResponse) {
        console.log("getFilterListByModuleAndType error");
        if (error.status === 200) {

          let jsonString=error.error.text;
          jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
          this.filterModuleMasterList=JSON.parse(jsonString);
        }else{
          this.authService.showMessage('error','Something went wrong', '');
      }

      }
    });
  }

  filterPerform(){
    this.getOkrMasterList();
  }

  resetForm(){
    this.okrMaster = new OkrMaster();
    this.uploadedFiles = [];
    this.toggleView();
    if(this.okrCsvFile != null) {
      this.okrCsvFile.clear();
    }
  }

  addNewOkrMaster(type:string){
    this.resetForm();
    // view add form
  }

  editOkrMaster(okrMaster: OkrMaster){
    this.resetForm();
    this.okrMaster = okrMaster;
  }

  viewDownloads(okrMaster: OkrMaster){
    this.okrMaster = okrMaster;
    this.modalDownloadLogs.show();
    this.getDownloadItemList();
  }

  getPredefinedListByType(type: string): any {
  //  this.spinnerService.show();
    let predefinedMaster = new PredefinedMaster();
    predefinedMaster.entityType = type;
    this.predefinedService.getPredefinedListByType(predefinedMaster).subscribe(
      data => {
        if (type === "INDUSTRY") {
          this.industryList = data;
        }else if(type === "DESIGNATION"){
          this.designationList = data;
        }else if(type === "DESIGNATION-LEVEL"){
          this.designationLevelList = data;
        }else if(type === "DEPARTMENT"){
          this.departmentList = data;
        }else if(type === "SUB-DEPARTMENT"){
          this.subDepartmentList = data;
        }
      },
      error => {
        if (error instanceof HttpErrorResponse) {
          if (error.status === 200) {
            let jsonString = error.error.text;
            jsonString = jsonString.substr(
              0,
              jsonString.indexOf('{"timestamp"')
            );
         //   this.spinnerService.hide();
            return JSON.parse(jsonString);
          } else {
            this.authService.showMessage('error','Something went wrong', '');
          //  this.spinnerService.hide();
          }
        }
      }
    );
  }

  getOkrMasterById(okrMaster:OkrMaster): any {
    this.spinnerService.show();
  
    this.okrService.getOkrMasterById(okrMaster).subscribe(
      data => {
        this.okrMaster = data;
        this.spinnerService.hide();
      },
      error => {
        if (error instanceof HttpErrorResponse) {
          if (error.status === 200) {
            let jsonString = error.error.text;
            jsonString = jsonString.substr(
              0,
              jsonString.indexOf('{"timestamp"')
            );
          //   this.spinnerService.hide();
            let data = JSON.parse(jsonString);
            okrMaster = data;
          } else {
            this.authService.showMessage('error','Something went wrong', '');
          //  this.spinnerService.hide();
          }
          this.spinnerService.hide();
        }
      }
    );
  }

  saveOkrMaster(){
    this.spinnerService.show();

    this.okrService.saveOkrMaster(this.okrMaster,this.uploadedFiles).subscribe(
      data => {
        this.spinnerService.hide();
        this.uploadedFiles = [];

        if(this.okrMaster.okrMasterId==null || this.okrMaster.okrMasterId==undefined || this.okrMaster.okrMasterId==0){
          this.authService.addToast({title:'', msg:'Saved Successfully', showClose: true, 
          timeout: 5000, theme:'default', type:'success', position:'bottom-left', closeOther:true});
          this.toggleView();
        }else{
          this.authService.addToast({title:'', msg:'Updated Successfully', showClose: true, 
          timeout: 5000, theme:'default', type:'success', position:'bottom-left', closeOther:true});
          this.getOkrMasterById(this.okrMaster);
        }

       
      },
      error => {
        if (error instanceof HttpErrorResponse) {
          if (error.status === 200) {
            let jsonString = error.error.text;
            jsonString = jsonString.substr(
              0,
              jsonString.indexOf('{"timestamp"')
            );
            this.spinnerService.hide();

            this.uploadedFiles = [];

            if(this.okrMaster.okrMasterId==null || this.okrMaster.okrMasterId==undefined || this.okrMaster.okrMasterId==0){
              this.authService.addToast({title:'', msg:'Saved Successfully', showClose: true, 
              timeout: 5000, theme:'default', type:'success', position:'bottom-left', closeOther:true});
              this.toggleView();
            }else{
              this.authService.addToast({title:'', msg:'Updated Successfully', showClose: true, 
              timeout: 5000, theme:'default', type:'success', position:'bottom-left', closeOther:true});
              this.getOkrMasterById(this.okrMaster);
            }

          } else {
            this.authService.showMessage('error','Something went wrong', '');
            
          //  this.spinnerService.hide();
          }
          this.spinnerService.hide();
        }
      }
    );

  }

  okrCsvFileUploader(event) {
    for(let file of event.files) {
      this.uploadedFiles = [];
      this.uploadedFiles.push(file);
    }
  }

  resetFilter(){
    this.okrMasterFilter = new OkrMaster();
    this.getOkrMasterList();
  }
  
  deleteOkrMaster(okrMaster:OkrMaster):void{

    swal({
      title: 'Are you sure ?',
      text: 'You wont be able to revert',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#C40000',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success m-r-10',
      cancelButtonClass: 'btn btn-primary',
      buttonsStyling: false
    }).then( (reponse)=>{ 
      console.log("dismiss "+reponse);
  
      if (reponse.dismiss!=null && reponse.dismiss!=undefined) {   
        swal(
          'Cancelled',
          '',
          'error'
        );

      }else{
      
        okrMaster.isDelete = 'Y';
        this.spinnerService.show();
        this.okrService.deleteOkrMaster(okrMaster).subscribe(data => {
         // this.authService.showMessage('success', 'Deleted Successfully', '');

        
         swal(
          'Delete !',
          'You Have Successfully Deleted',
          'success'
        );

        this.getOkrMasterList();
          this.spinnerService.hide();
        }, error => {
          if (error instanceof HttpErrorResponse) {
            if (error.status === 200) {
              // console.log(error.error.text);
              //json circular break at server side always add
              //extra information of timestamp and status along with
              //main json so parsing occurs still at status 200
             // this.authService.showMessage('success', 'Deleted Successfully', '');
            //  this.okrMaster.contentMasterList = this.okrMaster.contentMasterList.filter(data => data !== contentMaster);
             swal(
              'Delete !',
              'You Have Successfully Deleted',
              'success'
            );
           
            this.getOkrMasterList();
            
              this.spinnerService.hide();
            } else {
              this.authService.showMessage('error', 'Something Went wrong', '');
              this.spinnerService.hide();
            }
          }
        })

     
      }
      
    }).catch(swal.noop);
  }
  
  getDownloadItemList(){
  //  this.cardComponent.showLoader();
    this.spinnerService.show();
    this.getDownloadItemListLength();

    let okrMasterTemp = new OkrMaster();
    okrMasterTemp.okrMasterId = this.okrMaster.okrMasterId;
    this.downloadItemFilter.okrMaster = okrMasterTemp;
    this.downloadItemFilter.pageNumber=(this.downloadItemFilter.pageNumber == null) ? this.downloadItemPageNumber : this.downloadItemFilter.pageNumber ;
    this.downloadItemFilter.pageSize=(this.downloadItemFilter.pageSize == null) ? this.downloadItemPageSize : this.downloadItemFilter.pageSize ;

    this.downloadItemService.getDownloadItemList(this.downloadItemFilter).subscribe( data => {

      this.downloadItemList=data;
    this.spinnerService.hide();
  //     this.cardComponent.hideLoader();
    },error =>{
      if (error instanceof HttpErrorResponse) {

        if (error.status === 200) {
          // console.log(error.error.text);
          //json circular break at server side always add
          //extra information of timestamp and status along with
          //main json so parsing occurs still at status 200
          let jsonString=error.error.text;
          jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
          this.downloadItemList=JSON.parse(jsonString);
          this.spinnerService.hide();
        }else{
      //  this.authService.showMessage('error','Something Went Wrong','');
      //  this.spinnerService.hide();
      }
      this.spinnerService.hide();
    // this.cardComponent.hideLoader();
      }
    });
  }

  getDownloadItemListLength(){
  this.downloadItemService.getDownloadItemListLength(this.downloadItemFilter).subscribe( data => {
  this.downloadItemTotalRecords=data;
  },error =>{
    if (error instanceof HttpErrorResponse) {

      if (error.status === 200) {
        // console.log(error.error.text);
        //json circular break at server side always add
        //extra information of timestamp and status along with
        //main json so parsing occurs still at status 200
        let jsonString=error.error.text;
        jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
        this.downloadItemTotalRecords=JSON.parse(jsonString);
      }else{
    //   this.authService.showMessage('error','Something Went Wrong','');
    }

    }
  });
  }

  downloadItemPaginate(event) {
  this.downloadItemPageSize=event.rows;
  this.downloadItemFilter.pageNumber=event.page;
  this.downloadItemFilter.pageSize=this.downloadItemPageSize;
  this.getDownloadItemList();
  }

  isValidCSVFile(file: any) {  
    if(file.name.endsWith(".csv")) {
      return true;  
    } else {
      return false;
    }
  }  

  removeSpecialChars(result:string){

    let result1;

    result1 = result.replace("\"","");
    result1 = result1.replace("\"","");
    return result1;

  }

  uploadOkrCsvFile($event: any): void {  

    // Select the files from the event
    const files = $event.srcElement.files;
        
    if (this.isValidCSVFile(files[0])) {  
    
    
      // Parse the file you want to select for the operation along with the configuration
      this.ngxCsvParser.parse(files[0], { header: this.header, delimiter: ',' })
        .pipe().subscribe((result: Array<any>) => {

          this.getoutletCsvFileData(result);
          // this.csvRecords = result;
        }, (error: NgxCSVParserError) => {
          console.log('Error', error);
      });

    
  
    } else {  
      alert("Please import valid .csv file.");  
      this.okrCsvFileReset();  
    }  

  }

  okrCsvFileReset() {  
    this.okrCsvFileImport.nativeElement.value = "";  
  } 

  downloadOkrsampleFile(){
    this.downloadExcelReport(environment.okrDummyFilePath);
  }

  downloadExcelReport(fileUrl:string) {
    if(fileUrl === null){
        alert('No PDF to downlaod');
        return;
    }
    var fileUrlSplit = fileUrl.split("/");
    FileSaver.saveAs(fileUrl, fileUrlSplit[fileUrlSplit.length-1]);
  }

  getoutletCsvFileData(csvRecordsArray: any[]) {  
  
    for (let i = 1; i < csvRecordsArray.length; i++) {  
      // let curruntRecord = (<string>csvRecordsArray[i]).split(',');  
      let curruntRecord = csvRecordsArray[i];
      // console.log("curruntRecord[0]",curruntRecord[0][0]);
      // return;
      // if (curruntRecord.length == headerLength) {  

        let okrMaster = new OkrMaster();
        if(curruntRecord[0]) {
          okrMaster.objective = this.removeSpecialChars(curruntRecord[0]);
        } else {
          okrMaster.objective = "";
        }
        if(curruntRecord[1]) {
          okrMaster.keyResult = this.removeSpecialChars(curruntRecord[1]);
        } else {
          okrMaster.keyResult = "";
        }
        if(curruntRecord[2]) {
          okrMaster.industryNameTemp = this.removeSpecialChars(curruntRecord[2]);
        } else {
          okrMaster.industryNameTemp = "";
        }
        if(curruntRecord[3]) {
          okrMaster.designationNameTemp = this.removeSpecialChars(curruntRecord[3]);
        } else {
          okrMaster.designationNameTemp = "";
        }
        if(curruntRecord[4]) {
          okrMaster.designationLevelNameTemp = this.removeSpecialChars(curruntRecord[4]);
        } else {
          okrMaster.designationLevelNameTemp = "";
        }
        if(curruntRecord[5]) {
          okrMaster.departmentNameTemp = this.removeSpecialChars(curruntRecord[5]);
        } else {
          okrMaster.departmentNameTemp = "";
        }
        if(curruntRecord[6]) {
          okrMaster.subDepartmentNameTemp = this.removeSpecialChars(curruntRecord[6]);
        } else {
          okrMaster.subDepartmentNameTemp = "";
        }

        okrMaster.isDelete = "N";
        okrMaster.newlyAdded = true;
        okrMaster.okrMasterId = Math.floor(Math.random() * Math.floor(9999));
    
        this.importOkrMasterList.push(okrMaster);
      // }  
    }  

  } 

  uploadOkr(){
    this.importOkrMasterList = [];
    this.modalOkrImport.show();
  }

  onOkrEditDelete(okrMaster: OkrMaster){
    okrMaster.isDelete = "Y";
  }

  saveImportOkrMaster(){

    this.spinnerService.show();

    this.okrService.saveImportOkrMaster(this.importOkrMasterList).subscribe(
      data => {

        this.spinnerService.hide();
        this.modalOkrImport.hide();
        
        this.authService.addToast({title:'', msg:'Saved Successfully', showClose: true, 
        timeout: 5000, theme:'default', type:'success', position:'bottom-left', closeOther:true});
        this.getOkrMasterList();
       
      },
      error => {
        if (error instanceof HttpErrorResponse) {
          if (error.status === 200) {
            let jsonString = error.error.text;
            jsonString = jsonString.substr(
              0,
              jsonString.indexOf('{"timestamp"')
            );

            this.spinnerService.hide();
            this.modalOkrImport.hide();
            this.authService.addToast({title:'', msg:'Saved Successfully', showClose: true, 
            timeout: 5000, theme:'default', type:'success', position:'bottom-left', closeOther:true});
            this.getOkrMasterList();
    
          } else {
            this.authService.showMessage('error','Something went wrong', '');
            
          //  this.spinnerService.hide();
          }
          this.spinnerService.hide();
        }
      }
    );
  }

}
