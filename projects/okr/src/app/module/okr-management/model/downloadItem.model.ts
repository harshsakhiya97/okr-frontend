import { UserLogin } from "projects/userapp/src/app/module/user/model/userlogin.model";
import { OkrMaster } from "./okrmaster.model";

export class DownloadItem{
    public downloadItemId: number;
    public user: UserLogin;
    public userId: number;
    public okrMaster: OkrMaster;
    public okrMasterId: number;
    public isDelete: string;
    public pageSize: number;
    public pageNumber: number;
    public sortOrder: number;
    public lastDownloadItemId: number;
    public recordSize: number;
    

}