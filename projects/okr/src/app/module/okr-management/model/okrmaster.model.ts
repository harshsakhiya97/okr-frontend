import { ContentMaster } from './contentmaster.model';
import { LookupData } from 'projects/utility/src/app/module/common/filter/model/lookupdata.model';
import { PredefinedMaster } from 'src/app/shared/predefinedsettings/model/predefinedmaster.model';
import { UserLogin } from 'projects/userapp/src/app/module/user/model/userlogin.model';

export class OkrMaster {
    public okrMasterId: number;
    public industry:PredefinedMaster;
    public industryId:number;
    public designation:PredefinedMaster;
    public designationId:number;
    public designationLevel:PredefinedMaster;
    public designationLevelId:number;
    public department:PredefinedMaster;
    public departmentId:number;
    public subDepartment:PredefinedMaster;
    public subDepartmentId:number;    
    public objective: string;
    public keyResult: string;
    public isDelete: string;
    public isDeactive: string;
    public totalDownloads: number;
    public totalAccessed: number;
    public searchValue: string;
    public pageNumber:number;
    public sortOrder:number;
    public pageSize:number;

    public industryNameTemp:String;
    public designationNameTemp:String;
    public designationLevelNameTemp:String;
    public departmentNameTemp:String;
    public subDepartmentNameTemp:String;
    public newlyAdded:boolean;

}