import { UserLogin } from 'projects/userapp/src/app/module/user/model/userlogin.model';
import { OkrMaster } from './okrmaster.model';

export class ContentMaster {
    public contentId: number;
    public okrMaster: OkrMaster;
    public contentType: String;
    public fileName: String;
    public filePath: String;
    public createdDate: Date;
    public createdBy: UserLogin;
    public pageSize: number;
    public pageNumber: number;
    public file: File;
    public postId:number;
    public entityType:string;

}