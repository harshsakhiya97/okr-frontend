import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { OkrMaster } from '../model/okrmaster.model';
  
  @Injectable()
  export class OkrService {
  
    constructor(private http: HttpClient) { }

    saveOkrMaster(okrMaster:OkrMaster,files:any[]): Observable<any> {
       let formData:FormData = new FormData();
        for (let x = 0; x < files.length; x++) {
          formData.append("okrReportFileList",files[x] );
        };
        formData.append('okrMasterJson', new Blob([JSON.stringify(okrMaster)],
            {
                type: "application/json"
            }));
      return this.http.post<any>(environment.apiUrl+'/okr/saveOkr',formData);
    }

    saveImportOkrMaster(okrMasterList:OkrMaster[]): Observable<any> {
     return this.http.post<any>(environment.apiUrl+'/okr/saveImportOkrMaster',okrMasterList);
   }
    

    getOkrMasterById(okrMaster:OkrMaster): Observable<any> {
      return this.http.post<any>(environment.apiUrl+'/okr/getOkrById',okrMaster);
    }

    deleteOkrMaster(okrMaster:OkrMaster): Observable<any> {
        return this.http.post<any>(environment.apiUrl+'/okr/deleteOkr',okrMaster);
    }

    getOkrMasterList(okrMaster:OkrMaster): Observable<any> {
      return this.http.post<any>(environment.apiUrl+'/okr/getOkrMasterList',okrMaster);
  }

    getOkrMasterListLength(okrMaster:OkrMaster): Observable<any> {
        return this.http.post<any>(environment.apiUrl+'/okr/getOkrMasterListLength',okrMaster);
    }
 
}