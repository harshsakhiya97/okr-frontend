import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { DownloadItem } from '../model/downloadItem.model';
  
  @Injectable()
  export class DownloadItemService {
  
    constructor(private http: HttpClient) { }

    saveDownloadItem(downloadItem:DownloadItem): Observable<any> {
        return this.http.post<any>(environment.apiUrl+'/downloadItem/saveDownloadItem',downloadItem);
    }

    getDownloadItemById(downloadItem:DownloadItem): Observable<any> {
        return this.http.post<any>(environment.apiUrl+'/downloadItem/getDownloadItemById',downloadItem);
    }
  
    deleteDownloadItem(downloadItem:DownloadItem): Observable<any> {
        return this.http.post<any>(environment.apiUrl+'/downloadItem/deleteDownloadItem',downloadItem);
    }

    getDownloadItemList(downloadItem:DownloadItem): Observable<any> {
      return this.http.post<any>(environment.apiUrl+'/downloadItem/getDownloadItemList',downloadItem);
    }

    getDownloadItemListLength(downloadItem:DownloadItem): Observable<any> {
        return this.http.post<any>(environment.apiUrl+'/downloadItem/getDownloadItemListLength',downloadItem);
    }
 
}