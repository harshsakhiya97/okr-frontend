import { trigger, transition, style, animate } from '@angular/animations';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { OkrService } from 'projects/okr/src/app/module/okr-management/service/okr.service';
import { UserLogin } from 'projects/userapp/src/app/module/user/model/userlogin.model';
import { UserService } from 'projects/userapp/src/app/module/user/service/user.service';
import { AuthService } from 'projects/utility/src/app/module/service/auth.service';
import { PredefinedService } from 'src/app/shared/predefinedsettings/service/predefined.service';
import { DownloadItem } from '../okr-management/model/downloadItem.model';
import { OkrMaster } from '../okr-management/model/okrmaster.model';
import { DownloadItemService } from '../okr-management/service/downloadItem.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css'],
  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [
        style({opacity: 0}),
        animate('400ms ease-in-out', style({opacity: 1}))
      ]),
      transition(':leave', [
        style({transform: 'translate(0)'}),
        animate('400ms ease-in-out', style({opacity: 0}))
      ])
    ])
  ]
})
export class AdminDashboardComponent implements OnInit {
  @ViewChild('modalDownloadLogs') modalDownloadLogs:any;

  totalOkrRecords:number = 0;
  totalUserRecords:number = 0;
  totalDownloadItemRecords:number = 0;

  mostDownloadedTableSettings:any={};
  mostDownloadedTableCols:any[]=[];
  mostDownloadedList: OkrMaster[]=[];
  mostDownloadedTotalRecords: number=0;
  mostDownloadedFilter: OkrMaster = new OkrMaster();
  mostDownloadedPageSize: number=10;
  mostDownloadedPageNumber: number=0;

  mostAccessedTableSettings:any={};
  mostAccessedTableCols:any[]=[];
  mostAccessedList: OkrMaster[]=[];
  mostAccessedTotalRecords: number=0;
  mostAccessedFilter: OkrMaster = new OkrMaster();
  mostAccessedPageSize: number=10;
  mostAccessedPageNumber: number=0;

  downloadItemFilter: DownloadItem = new DownloadItem();
  downloadItemTableSettings:any={};
  downloadItemTableCols:any[]=[];
  downloadItemList: DownloadItem[]=[];
  downloadItemTotalRecords: number=0;
  downloadItemPageSize: number=10;
  downloadItemPageNumber: number=0;

  viewOkrMaster: OkrMaster = new OkrMaster();

  constructor(private okrService:OkrService,private authService:AuthService,
    private downloadItemService:DownloadItemService,private userService:UserService,
    private spinnerService: NgxSpinnerService) { // private servicePNotify: NotificationsService
  }
  
  ngOnInit() {

    this.getOkrMasterListLength();
    this.getUserListLength();
    this.getDownloadItemListLength();
    this.setTableSettings();
    this.getMostDownloadedList();
    this.getMostAccessedList();

  }

  setTableSettings(){

    let actionSettings = {
      download: true,
    };

    this.mostDownloadedTableCols=[ 
      {filterfield:'action',field:'action',hidden:true, header:'Action',width : '60px',fieldType:'na',columnType:'action',actionSettings:actionSettings},  
      {filterfield:'objective',field:'objective',header:'Objective',width : '200px',fieldType:'text',columnType:'data'},
      {filterfield:'keyResult',field:'keyResult',header:'Key Result',width : '250px',fieldType:'text',columnType:'data'},
      {filterfield:'totalDownloads',field:'totalDownloads',header:'Total Downloads',width : '100px',fieldType:'text',columnType:'data'},
      {filterfield:'totalAccessed',field:'totalAccessed',header:'Total Accessed',width : '100px',fieldType:'text',columnType:'data'},
      {filterfield:'industry.name',field:'industry',subfield:'name',header:'Industry',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'designation.name',field:'designation',subfield:'name',header:'Designation',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'designationLevel.name',field:'designationLevel',subfield:'name',header:'Designation Level',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'department.name',field:'department',subfield:'name',header:'Department',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'subDepartment.name',field:'subDepartment',subfield:'name',header:'Sub Department',width : '120px',fieldType:'text',columnType:'data'},
    ];

    this.mostDownloadedTableSettings = {
      autoLayout: true,
      sortMode: "multiple",
      scrollable: true,
      width: "350%",
      scrollHeight: "600px",
      reorderableColumns: true,
      resizableColumns: true,
      tableFilter:false,
      paginate:true
    };

    
    this.mostAccessedTableCols=[ 
      {filterfield:'action',field:'action',hidden:true, header:'Action',width : '60px',fieldType:'na',columnType:'action',actionSettings:actionSettings},  
      {filterfield:'objective',field:'objective',header:'Objective',width : '200px',fieldType:'text',columnType:'data'},
      {filterfield:'keyResult',field:'keyResult',header:'Key Result',width : '250px',fieldType:'text',columnType:'data'},
      {filterfield:'totalDownloads',field:'totalDownloads',header:'Total Downloads',width : '100px',fieldType:'text',columnType:'data'},
      {filterfield:'totalAccessed',field:'totalAccessed',header:'Total Accessed',width : '100px',fieldType:'text',columnType:'data'},
      {filterfield:'industry.name',field:'industry',subfield:'name',header:'Industry',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'designation.name',field:'designation',subfield:'name',header:'Designation',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'designationLevel.name',field:'designationLevel',subfield:'name',header:'Designation Level',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'department.name',field:'department',subfield:'name',header:'Department',width : '120px',fieldType:'text',columnType:'data'},
      {filterfield:'subDepartment.name',field:'subDepartment',subfield:'name',header:'Sub Department',width : '120px',fieldType:'text',columnType:'data'},
    ];

    this.mostAccessedTableSettings = {
      autoLayout: true,
      sortMode: "multiple",
      scrollable: true,
      width: "350%",
      scrollHeight: "500px",
      reorderableColumns: true,
      resizableColumns: true,
      tableFilter:false,
      paginate:true
    };

    this.downloadItemTableCols=[ 
      {filterfield:'createdDate',field:'createdDate',header:'Download Date',width : '70px',fieldType:'date',columnType:'data',dateFormat:'medium'},
      {filterfield:'user.fullName',field:'user',subfield:'fullName',header:'User',width : '70px',fieldType:'text',columnType:'data'},
    ];

    this.downloadItemTableSettings = {
      autoLayout: true,
      sortMode: "multiple",
      scrollable: true,
      width: "350%",
      scrollHeight: "500px",
      reorderableColumns: true,
      resizableColumns: true,
      tableFilter:false,
      paginate:true
    };

  }

  
  getMostDownloadedList(){
    //  this.cardComponent.showLoader();
      this.spinnerService.show();
  
      this.mostDownloadedFilter.sortOrder = 1;
      this.mostDownloadedFilter.pageNumber=(this.mostDownloadedFilter.pageNumber == null) ? this.mostDownloadedPageNumber : this.mostDownloadedFilter.pageNumber ;
      this.mostDownloadedFilter.pageSize=(this.mostDownloadedFilter.pageSize == null) ? this.mostDownloadedPageSize : this.mostDownloadedFilter.pageSize ;
  
      this.getMostDownloadedListLength();
      this.okrService.getOkrMasterList(this.mostDownloadedFilter).subscribe( data => {
  
        this.mostDownloadedList=data;
        this.spinnerService.hide();
    //     this.cardComponent.hideLoader();
      },error =>{
        if (error instanceof HttpErrorResponse) {
  
          if (error.status === 200) {
            // console.log(error.error.text);
            //json circular break at server side always add
            //extra information of timestamp and status along with
            //main json so parsing occurs still at status 200
            let jsonString=error.error.text;
            jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
            this.mostDownloadedList=JSON.parse(jsonString);
            this.spinnerService.hide();
          }else{
        //  this.authService.showMessage('error','Something Went Wrong','');
        //  this.spinnerService.hide();
        }
        this.spinnerService.hide();
      // this.cardComponent.hideLoader();
        }
      });
  }
  
  getMostDownloadedListLength(){
    this.okrService.getOkrMasterListLength(this.mostDownloadedFilter).subscribe( data => {
    this.mostDownloadedTotalRecords=data;
    },error =>{
      if (error instanceof HttpErrorResponse) {
  
        if (error.status === 200) {
          // console.log(error.error.text);
          //json circular break at server side always add
          //extra information of timestamp and status along with
          //main json so parsing occurs still at status 200
          let jsonString=error.error.text;
          jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
          this.mostDownloadedTotalRecords=JSON.parse(jsonString);
        }else{
      //   this.authService.showMessage('error','Something Went Wrong','');
      }
  
      }
    });
  }

  paginateMostDownloaded(event) {
  this.mostDownloadedFilter.pageNumber=event.page;
  this.mostDownloadedFilter.pageSize=event.rows;
  this.getMostDownloadedList();
  }

  getMostAccessedList(){
    //  this.cardComponent.showLoader();
      this.spinnerService.show();
  
      this.mostAccessedFilter.sortOrder = 2;
      this.mostAccessedFilter.pageNumber=(this.mostAccessedFilter.pageNumber == null) ? this.mostAccessedPageNumber : this.mostAccessedFilter.pageNumber ;
      this.mostAccessedFilter.pageSize=(this.mostAccessedFilter.pageSize == null) ? this.mostAccessedPageSize : this.mostAccessedFilter.pageSize ;
  
      this.getMostAccessedListLength();
      this.okrService.getOkrMasterList(this.mostAccessedFilter).subscribe( data => {
  
        this.mostAccessedList=data;
        this.spinnerService.hide();
    //     this.cardComponent.hideLoader();
      },error =>{
        if (error instanceof HttpErrorResponse) {
  
          if (error.status === 200) {
            // console.log(error.error.text);
            //json circular break at server side always add
            //extra information of timestamp and status along with
            //main json so parsing occurs still at status 200
            let jsonString=error.error.text;
            jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
            this.mostAccessedList=JSON.parse(jsonString);
            this.spinnerService.hide();
          }else{
        //  this.authService.showMessage('error','Something Went Wrong','');
        //  this.spinnerService.hide();
        }
        this.spinnerService.hide();
      // this.cardComponent.hideLoader();
        }
      });
  }
  
  getMostAccessedListLength(){
    this.okrService.getOkrMasterListLength(this.mostAccessedFilter).subscribe( data => {
    this.mostAccessedTotalRecords=data;
    },error =>{
      if (error instanceof HttpErrorResponse) {
  
        if (error.status === 200) {
          // console.log(error.error.text);
          //json circular break at server side always add
          //extra information of timestamp and status along with
          //main json so parsing occurs still at status 200
          let jsonString=error.error.text;
          jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
          this.mostAccessedTotalRecords=JSON.parse(jsonString);
        }else{
      //   this.authService.showMessage('error','Something Went Wrong','');
      }
  
      }
    });
  }

  paginateMostAccessed(event) {
    this.mostAccessedFilter.pageNumber=event.page;
    this.mostAccessedFilter.pageSize=event.rows;
    this.getMostAccessedList();
  }

  viewDownloads(okrMaster: OkrMaster){
    this.viewOkrMaster = okrMaster;
    this.modalDownloadLogs.show();
    this.getDownloadItemList();
  }

  getDownloadItemList(){
    //  this.cardComponent.showLoader();
      this.spinnerService.show();
      this.getDownloadItemViewListLength();
  
      let okrMasterTemp = new OkrMaster();
      okrMasterTemp.okrMasterId = this.viewOkrMaster.okrMasterId;
      this.downloadItemFilter.okrMaster = okrMasterTemp;
      this.downloadItemFilter.pageNumber=(this.downloadItemFilter.pageNumber == null) ? this.downloadItemPageNumber : this.downloadItemFilter.pageNumber ;
      this.downloadItemFilter.pageSize=(this.downloadItemFilter.pageSize == null) ? this.downloadItemPageSize : this.downloadItemFilter.pageSize ;
  
      this.downloadItemService.getDownloadItemList(this.downloadItemFilter).subscribe( data => {
  
        this.downloadItemList=data;
      this.spinnerService.hide();
    //     this.cardComponent.hideLoader();
      },error =>{
        if (error instanceof HttpErrorResponse) {
  
          if (error.status === 200) {
            // console.log(error.error.text);
            //json circular break at server side always add
            //extra information of timestamp and status along with
            //main json so parsing occurs still at status 200
            let jsonString=error.error.text;
            jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
            this.downloadItemList=JSON.parse(jsonString);
            this.spinnerService.hide();
          }else{
        //  this.authService.showMessage('error','Something Went Wrong','');
        //  this.spinnerService.hide();
        }
        this.spinnerService.hide();
      // this.cardComponent.hideLoader();
        }
      });
    }
  
    getDownloadItemViewListLength(){
    this.downloadItemService.getDownloadItemListLength(this.downloadItemFilter).subscribe( data => {
    this.downloadItemTotalRecords=data;
    },error =>{
      if (error instanceof HttpErrorResponse) {
  
        if (error.status === 200) {
          // console.log(error.error.text);
          //json circular break at server side always add
          //extra information of timestamp and status along with
          //main json so parsing occurs still at status 200
          let jsonString=error.error.text;
          jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
          this.downloadItemTotalRecords=JSON.parse(jsonString);
        }else{
      //   this.authService.showMessage('error','Something Went Wrong','');
      }
  
      }
    });
    }
  
    downloadItemPaginate(event) {
    this.downloadItemPageSize=event.rows;
    this.downloadItemFilter.pageNumber=event.page;
    this.downloadItemFilter.pageSize=this.downloadItemPageSize;
    this.getDownloadItemList();
    }

  /// stats
  getOkrMasterListLength(){
    this.okrService.getOkrMasterListLength(new OkrMaster()).subscribe( data => {
    this.totalOkrRecords=data;
    },error =>{
      if (error instanceof HttpErrorResponse) {
  
        if (error.status === 200) {
          // console.log(error.error.text);
          //json circular break at server side always add
          //extra information of timestamp and status along with
          //main json so parsing occurs still at status 200
          let jsonString=error.error.text;
          jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
          this.totalOkrRecords=JSON.parse(jsonString);
        }else{
      //   this.authService.showMessage('error','Something Went Wrong','');
      }
  
      }
    });
  }

  getUserListLength(){
    this.userService.getUserListLength(new UserLogin()).subscribe( data => {
     this.totalUserRecords=data;
    },error =>{
      if (error instanceof HttpErrorResponse) {
  
        if (error.status === 200) {
           // console.log(error.error.text);
           //json circular break at server side always add
           //extra information of timestamp and status along with
           //main json so parsing occurs still at status 200
           let jsonString=error.error.text;
           jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
           this.totalUserRecords=JSON.parse(jsonString);
        }else{
       //   this.authService.showMessage('error','Something Went Wrong','');
      }
  
      }
    });
  }

  getDownloadItemListLength(){
    this.downloadItemService.getDownloadItemListLength(new DownloadItem()).subscribe( data => {
    this.totalDownloadItemRecords=data;
    },error =>{
      if (error instanceof HttpErrorResponse) {
  
        if (error.status === 200) {
          // console.log(error.error.text);
          //json circular break at server side always add
          //extra information of timestamp and status along with
          //main json so parsing occurs still at status 200
          let jsonString=error.error.text;
          jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
          this.totalDownloadItemRecords=JSON.parse(jsonString);
        }else{
      //   this.authService.showMessage('error','Something Went Wrong','');
      }
  
      }
    });
  }
}