import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OkrRoutingModule } from './okr-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AdminDashboardComponent } from './module/admin-dashboard/admin-dashboard.component';
import { OkrManagementComponent } from './module/okr-management/okr-management.component';
import { CommonModule } from '@angular/common';
import { TabViewModule } from 'primeng/tabview';
import { UtilityModule } from 'projects/utility/src/app/utility.module';
import { DownloadItemService } from './module/okr-management/service/downloadItem.service';

@NgModule({
  declarations: [
    AppComponent,
    AdminDashboardComponent,
    OkrManagementComponent
  ],
  imports: [
    CommonModule,
    TabViewModule,
    UtilityModule,
    SharedModule,
    OkrRoutingModule
  ],
  providers: [DownloadItemService],
  bootstrap: [AppComponent]
})
export class OkrModule { }
