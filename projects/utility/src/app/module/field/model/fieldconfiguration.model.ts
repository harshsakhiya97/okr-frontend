import { FieldValidation } from './fieldvalidation.model';

export class FieldConfiguration {
    public fieldConfigurationId: number;
    public entityClient: string;
    public entityServer: string;
    public sectionName: string;
    public fieldName: string;
    public fieldType: string;
    public label: string;
    public orderNumber: number;
    public fieldCondition1: string;
    public fieldCondition2: string;
    public isDelete: string;
    public fieldValidation: FieldValidation;

    public subSectionName: string;
    public columnClass: string;

    public pageNumber: number;
    public pageSize: number;
}