import { FieldConfiguration } from './fieldconfiguration.model';

export class FieldValidation {
    public fieldValidationId: number;
    public mandatoryField: boolean;
    public checkRegex: boolean;
    public checkRegexMsg: string;
    public regexPattern: string;
    public fieldConfiguration: FieldConfiguration;

}