import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'Application/json; charset=UTF-8'
  }),
  responseType: 'blob' as 'blob'
};

const httpOptionsfile = {
  headers: new HttpHeaders({
   'Access-Control-Allow-Origin':'*',
   'Accept': 'application/json'})
};

@Injectable()
  export class LookupService {

  constructor(private http: HttpClient) { }

  getRoleList(): Observable<any> {
    return this.http.get<any>(environment.apiUrl+'/role/getRoleList');
  }



}
