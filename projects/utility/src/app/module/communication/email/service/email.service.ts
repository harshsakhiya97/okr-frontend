import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { EmailData } from '../model/emaildata.model';
  
  @Injectable()
  export class EmailService {
  
  
    constructor(private http: HttpClient) { }
  
    sendDirectEmail(emailData:EmailData): Observable<any> {

        let formData:FormData = new FormData();
 
        // console.log("In upload!!!");
       
       if(emailData.file!=null && emailData.file!=undefined){
        for (let x = 0; x < emailData.file.length; x++) {
            formData.append("attachments",emailData.file[x] );
        };
         
         formData.append('mail', JSON.stringify(emailData));
       }else{
         formData.append('mail', JSON.stringify(emailData));
       }
       

      return this.http.post<any>(environment.apiUrl+'/email/sendDirectEmail',formData);
    }
    
      
  }