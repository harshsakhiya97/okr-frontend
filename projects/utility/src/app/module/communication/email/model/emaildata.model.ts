
export class EmailData {
    public to:string;
    public from:string;
    public subject:string;
    public content:string;
    public templateCode:string;
    public sendingType:string;
    public file:File[];
    
}
