import { Component, OnInit, ViewChild } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { NgxSpinnerService } from 'ngx-spinner';
import swal from 'sweetalert2';
import { PredefinedMaster } from './model/predefinedmaster.model';
import { AuthService } from 'projects/utility/src/app/module/service/auth.service';
import { PredefinedService } from './service/predefined.service';
import { trigger, transition, style, animate } from '@angular/animations';

interface dropdownValue{
  "name":string;
}

@Component({
  selector: "app-predefinedsettings",
  templateUrl: "./predefinedsettings.component.html",
  styleUrls: ["./predefinedsettings.component.css"],
  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [
        style({opacity: 0}),
        animate('400ms ease-in-out', style({opacity: 1}))
      ]),
      transition(':leave', [
        style({transform: 'translate(0)'}),
        animate('400ms ease-in-out', style({opacity: 0}))
      ])
    ])
  ]
})
export class PredefinedsettingsComponent implements OnInit {
  @ViewChild('modalPredefined') modalPredefined:any;
  predefinedMaster: PredefinedMaster = new PredefinedMaster();
  name: string;

  totalIndustryRecords: number = 0;
  totalDesignationRecords: number = 0;
  totalDesignationLevelRecords: number = 0;
  totalDepartmentRecords: number = 0;
  totalSubDepartmentRecords: number = 0;
  totalRegionRecords: number = 0;

  masterCols: any[];
  subDepartmentCols: any[];
  industryList: PredefinedMaster[]=[];
  designationList: PredefinedMaster[]=[];
  designationLevelList: PredefinedMaster[]=[];
  departmentList: PredefinedMaster[]=[];
  subDepartmentList: PredefinedMaster[]=[];
  regionList: PredefinedMaster[]=[];

  constructor(
    private predefinedService: PredefinedService,
    private authService: AuthService,
    private spinnerService: NgxSpinnerService
  ) {}

  ngOnInit() {
      this.getPredefinedListByType("INDUSTRY");
      this.getPredefinedListByType("DESIGNATION");
      this.getPredefinedListByType("DESIGNATION-LEVEL");
      this.getPredefinedListByType("DEPARTMENT");
      this.getPredefinedListByType("SUB-DEPARTMENT");
      this.getPredefinedListByType("REGION");
      this.setTableSetting();
  }

  setTableSetting(){

    this.masterCols=[   
      {filterfield:'name',field:'name',header:'Name',width : '300px',fieldType:'text',columnType:'data'}, 
    ];

    this.subDepartmentCols=[   
      {filterfield:'parentName',field:'parentName',header:'Department Name',width : '300px',fieldType:'text',columnType:'data'}, 
      {filterfield:'name',field:'name',header:'Sub Department Name',width : '300px',fieldType:'text',columnType:'data'}, 
    ];

  } 
 

  getPredefinedListByType(type: string): any {
    let predefinedMaster = new PredefinedMaster();
    predefinedMaster.entityType = type;
    this.predefinedService.getPredefinedListByType(predefinedMaster).subscribe(
      data => {
        if (type === "INDUSTRY") {
          this.industryList = data;
          this.totalIndustryRecords = data.length;
        }else if (type === "DESIGNATION") {
          this.designationList = data;
          this.totalDesignationRecords = data.length;
        }else if (type === "DESIGNATION-LEVEL") {
          this.designationLevelList = data;
          this.totalDesignationLevelRecords = data.length;
        }else if (type === "DEPARTMENT") {
          this.departmentList = data;
          this.totalDepartmentRecords = data.length;
        }else if (type === "SUB-DEPARTMENT") {
          this.subDepartmentList = data;
          this.totalSubDepartmentRecords = data.length;
        }else if (type === "REGION") {
          this.regionList = data;
          this.totalRegionRecords = data.length;
        }

      },
      error => {
        if (error instanceof HttpErrorResponse) {
          if (error.status === 200) {
            let jsonString = error.error.text;
            jsonString = jsonString.substr(
              0,
              jsonString.indexOf('{"timestamp"')
            );
         //   this.spinnerService.hide();
            return JSON.parse(jsonString);
          } else {
            this.authService.showMessage('error','Something went wrong', '');
          //  this.spinnerService.hide();
          }
        }
      }
    );
  }

  savePredefined() {
    this.spinnerService.show();
    let validateCheckStr = "";
    let validateCheck = false;

    if(this.predefinedMaster.name === null || this.predefinedMaster.name === undefined || this.predefinedMaster.name === ""){
        validateCheck = true;
        validateCheckStr += "Name  ";
    }

    if(validateCheck){
       validateCheckStr += " mandatory!!! ";

    this.authService.addToast({title:'', msg:validateCheckStr, showClose: true, 
    timeout: 5000, theme:'default', type:'error', position:'bottom-left', closeOther:true});
    
    this.spinnerService.hide();
    return;
   }

    this.predefinedService.savePredefinedMaster(this.predefinedMaster).subscribe(
      data => {
        this.authService.addToast({title:'', msg:'Saved Successfully', showClose: true, 
        timeout: 5000, theme:'default', type:'success', position:'bottom-left', closeOther:true});

        this.getPredefinedListByType(data.entityType);
        this.modalPredefined.hide();
        this.spinnerService.hide();
      },
      error => {
        if (error instanceof HttpErrorResponse) {
          if (error.status === 500) {
            // console.log(error.error.text);
            //json circular break at server side always add
            //extra information of timestamp and status along with
            //main json so parsing occurs still at status 200
            this.spinnerService.hide();
          } else {
            this.authService.showMessage('error','Something Went Wrong','');
            this.spinnerService.hide();
          }
          this.modalPredefined.hide();
        }
      }
    );
  }

  addPredefinedModal(entity:string){
    this.predefinedMaster = new PredefinedMaster(); 
    this.predefinedMaster.entityType = entity;
    this.predefinedMaster.rowId=Math.random()+"";  
    this.modalPredefined.show();
  }
  updatePredefinedModal(updatePredefinedMaster: PredefinedMaster){
    this.predefinedMaster = updatePredefinedMaster; 
    this.modalPredefined.show();
  }


  updatePredefinedList(){
    this.spinnerService.show();
    this.predefinedService.savePredefinedMaster(this.predefinedMaster).subscribe(
      data => {
        this.authService.addToast({title:'', msg:'Updated Successfully', showClose: true, 
        timeout: 5000, theme:'default', type:'error', position:'bottom-left', closeOther:true});

        this.getPredefinedListByType(data.entityType);
        this.spinnerService.hide();
      },
      error => {
        if (error instanceof HttpErrorResponse) {
          if (error.status === 500) {
            // console.log(error.error.text);
            //json circular break at server side always add
            //extra information of timestamp and status along with
            //main json so parsing occurs still at status 200
            this.spinnerService.hide();
          } else {
            this.authService.showMessage('error','Something Went Wrong','');
            this.spinnerService.hide();
          }
        }
      }
    );
  }

  deletePredefinedMaster(predefinedMaster: PredefinedMaster): void {

    let message=" delete";
    
    swal({
      title: 'Are you sure, '+message+' ?',
      text: 'You wont be able to revert',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#C40000',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, '+message+' it!',
      cancelButtonText: 'No, cancel!',
      confirmButtonClass: 'btn btn-success m-r-10',
      cancelButtonClass: 'btn btn-primary',
      buttonsStyling: false
    }).then( (reponse)=>{ 
      console.log("dismiss "+reponse);
  
      if (reponse.dismiss!=null && reponse.dismiss!=undefined) {   
        swal(
          'Cancelled',
          '',
          'error'
        );

      }else{
      

        this.spinnerService.show();
        this.predefinedService.deletePredefinedMaster(predefinedMaster).subscribe(data => {
         // this.authService.showMessage('success', 'Deleted Successfully', '');
         swal(
          message+' !',
          'You Have Successfully Deleted',
          'success'
        );

            this.getPredefinedListByType(predefinedMaster.entityType);
            this.spinnerService.hide();
        }, error => {
          if (error instanceof HttpErrorResponse) {
            if (error.status === 200) {

              this.getPredefinedListByType(predefinedMaster.entityType);
             swal(
              message+' !',
              'You Have Successfully Deleted',
              'success'
            );
            
              this.spinnerService.hide();
            } else {
              this.authService.showMessage('error', 'Something Went wrong', '');
              this.spinnerService.hide();
            }
          }
        })

     
      }
      
    }).catch(swal.noop);

  }

}