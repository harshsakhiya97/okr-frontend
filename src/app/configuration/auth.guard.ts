import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SessionClient } from './sessionclient.storage';
 
@Injectable()
export class AuthGuard implements CanActivate {
 
    constructor(private router: Router,private sessionClient:SessionClient) { }
 
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.sessionClient.getToken()!=null) {
            // logged in so return true
            return true;
        }
 
        // not logged in so redirect to login page with the return url
       // this.router.navigateByUrl('/login');
        this.router.navigateByUrl('/'); 
        return false;
    }
}