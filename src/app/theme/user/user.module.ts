import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserRoutingModule} from './user-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {GMapModule} from 'primeng/gmap';

@NgModule({ 
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule,
    GMapModule
  ],
  declarations: [
]
})
export class UserModule { }
