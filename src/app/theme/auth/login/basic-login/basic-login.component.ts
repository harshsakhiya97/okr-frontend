import { Component, OnInit, OnDestroy, ElementRef, ViewChild, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

import {SessionClient} from '../../../../../../src/app/configuration/sessionclient.storage';
import {AuthService} from '../../../../../../projects/utility/src/app/module/service/auth.service';
import {FieldConfigurationService} from '../../../../../../projects/utility/src/app/module/field/service/fieldconfiguration.service';
import { AccessControlMaster } from 'projects/utility/src/app/module/system/model/accesscontrolmaster.model';
import { FieldConfiguration } from 'projects/utility/src/app/module/field/model/fieldconfiguration.model';
import { PropertiesDTO } from 'projects/utility/src/app/module/system/model/propertiesdto.model';
import { UserService } from 'projects/userapp/src/app/module/user/service/user.service';
import { UserLogin } from 'projects/userapp/src/app/module/user/model/userlogin.model';

declare global {
  interface Window {
    RTCPeerConnection: RTCPeerConnection;
    mozRTCPeerConnection: RTCPeerConnection;
    webkitRTCPeerConnection: RTCPeerConnection;
  }
}

@Component({
  selector: 'app-basic-login',
  templateUrl: './basic-login.component.html',
  styleUrls: ['./basic-login.component.scss']
})
export class BasicLoginComponent implements OnInit, OnDestroy {

  @ViewChild('forgotPasswordModalClose') forgotPasswordModalClose:any;
  //@ViewChild('forgetPasswordModalClose') forgotPasswordModalClose:ElementRef;
  user:UserLogin=new UserLogin();
  isLoginError:boolean=false;
  accessControlMasterList:AccessControlMaster[]=[];
  accessControlMasterMap: { [moduleCode: string]: AccessControlMaster; } = { };
  loginErrorMsg:string="";
  localIpAddress:string="";
  publicIpAddress:string="";
  fieldConfigurationList:FieldConfiguration[];
  //ipRegex = new RegExp(/([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/);
  properties:PropertiesDTO=new PropertiesDTO();
  userOtp:string="";
  forgotPassScreenIndex: number = 1;
  forgotUser:UserLogin=new UserLogin();
  confirmUser:UserLogin=new UserLogin();
  otp:string="";
  constructor(private router: Router, private authService: AuthService,
    private sessionClient: SessionClient,private zone: NgZone
    ,private fieldConfigurationService:FieldConfigurationService,
    private userService:UserService ) {
  }

  ngOnInit() {
    document.querySelector('body').setAttribute('themebg-pattern', 'theme1');
  
    if(this.sessionClient.getToken()!=null){
      console.log("redirect "+this.sessionClient.getToken());
      this.redirection();
    }
    //this.getSystemProperties();
    //this.authService.getLocalIpAddress();
   // this.authService.getPublicIpAddress();
    let fieldConfiguration=new FieldConfiguration();
    fieldConfiguration.entityServer="UserDto"
    this.getFieldConfigurationListByEntity(fieldConfiguration);
    //this.getEmployeeCompanyList();
   // this.getEmployeeBranchList();
    //this.getEmployeeDepartmentList();
   // this.getEmployeeDesignationList();

  }

  public ngOnDestroy() {
    document.body.className = '';
  }


  validateUser(): void {
    let validationStr="";
    let validationCheck=false;
    let term = "sample1@fs$%%";
    let re = new RegExp("^([a-z0-9]{5,})$");
    if (re.test(term)) {
        console.log("Valid");
    } else {
        console.log("Invalid");
    }


    for(let fieldConfiguration of this.fieldConfigurationList){
            if(fieldConfiguration.fieldValidation!==null && fieldConfiguration.fieldValidation!==undefined &&
              fieldConfiguration.fieldValidation.mandatoryField===true && (
                this[fieldConfiguration.entityClient][fieldConfiguration.fieldName]===undefined ||
                this[fieldConfiguration.entityClient][fieldConfiguration.fieldName]===null ||
                this[fieldConfiguration.entityClient][fieldConfiguration.fieldName]===""
              )){
                validationStr=validationStr+fieldConfiguration.label+" is mandatory, ";
                validationCheck=true;
            }
            console.log("check regex");
            if(fieldConfiguration.fieldValidation!==null && fieldConfiguration.fieldValidation!==undefined &&
              fieldConfiguration.fieldValidation.checkRegex===true &&
                this[fieldConfiguration.entityClient][fieldConfiguration.fieldName]!==undefined &&
                this[fieldConfiguration.entityClient][fieldConfiguration.fieldName]!==null &&
                this[fieldConfiguration.entityClient][fieldConfiguration.fieldName]!==""
             && fieldConfiguration.fieldValidation.regexPattern!==null && fieldConfiguration.fieldValidation.regexPattern!==undefined &&
            fieldConfiguration.fieldValidation.regexPattern!==""){

              //  let regexPattern=fieldConfiguration.fieldValidation.regexPattern;
                let regexPattern = new RegExp(fieldConfiguration.fieldValidation.regexPattern);
                if(regexPattern.test(this[fieldConfiguration.entityClient][fieldConfiguration.fieldName])){
                  validationStr=validationStr+fieldConfiguration.label +" "+ fieldConfiguration.fieldValidation.checkRegexMsg+" , ";
                  validationCheck=true;
                }


              }


    }
    if(validationCheck){
      this.authService.showMessage("error","Validation",validationStr);
     // alert(validationStr);
      return;
    }


    console.log("validateUser call");
    this.authService.attemptAuth(this.user)
      .subscribe( data  =>{
            this.sessionClient.saveToken(data.token);
            console.log("Token->"+data.token);
           // this.fetchUserDetail();
           this.getSystemProperties();
            console.log("validateUser inner");

      },(error: HttpErrorResponse)=>{
        this.sessionClient.signOut();
        if(error.status===401){
          this.loginErrorMsg="Invalid Username or Password";
          console.log("ERROR LOGIN "+error.error.message);
        }else if(error.status===403){
         // this.loginErrorMsg="Access Denied";
         this.getSystemProperties();
        }else{
          this.loginErrorMsg="Access Denied";
          console.log("ERROR LOGIN "+error.message);
        }

      //  console.log("user not found");
        //this.isLoginError=true;

      }

    )
  };

  /*fetchEmployeeByUsername(){
    console.log("fetchEmployeeByUsername");
        this.employeeService.fetchEmployeeByUsername(this.user).subscribe( data => {
          this.authService.setEmployeeSession(data);
           this.redirection();
        },error =>{
          if (error instanceof HttpErrorResponse) {
            if (error.status === 200) {
               let jsonString=error.error.text;
               jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
            }else{
          }

          }
        });
  }*/


  forgetPassword():void
  {

    this.fieldConfigurationService.forgetPassword(this.user).subscribe( data  =>{

      this.authService.showMessage('success', 'New Password Send to your registered Email id', '');

    },(err: HttpErrorResponse)=>{
      if (err.status === 200) {
        //json circular break at server side always add
            //extra information of timestamp and status along with
            //main json so parsing occurs still at status 200
            let jsonString=err.error.text;
            jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
            this.user=JSON.parse(jsonString);
            this.authService.setUserSession(this.user);
      }
      else
      {
        this.authService.showMessage('error', 'Invalid User ID', '');
      }
    });

  }

  fetchUserDetail():void{
    this.properties=this.authService.getProperties();
    this.authService.fetchUserDetail(this.user).subscribe( data  =>{
      
      this.user=data;
      //this.user.connected=true;
      this.authService.setUserSession(this.user);
      
      if(this.user.accessControlMasterList!==null && this.user.accessControlMasterList!==undefined &&
        this.user.accessControlMasterList.length!==0 ){
          this.authService.setAccessControlData(this.user.accessControlMasterList);
          this.accessControlMasterMap=this.authService.getAccessControlMapData();

          if((this.accessControlMasterMap['LOGIN']!==null &&
            this.accessControlMasterMap['LOGIN']!==undefined &&
            this.accessControlMasterMap['LOGIN'].readAccess) || true){

                if(this.properties.masterPropertiesDTO!==null &&
                  this.properties.masterPropertiesDTO!==undefined &&
                  this.properties.masterPropertiesDTO.checkIpAddress){

                    if(this.accessControlMasterMap['LOGIN'].staticIpAddress===null ||
                    this.accessControlMasterMap['LOGIN'].staticIpAddress===""){
                   //   this.fetchEmployeeByUsername();
                      this.redirection();
                    }else if(this.accessControlMasterMap['LOGIN'].staticIpAddress.includes(this.publicIpAddress) ||
                    this.accessControlMasterMap['LOGIN'].staticIpAddress.includes(this.localIpAddress)){
                      console.log("ip matched public ");
                    //  this.fetchEmployeeByUsername();
                      this.redirection();
                    }else{
                    //  this.isLoginError=true;
                      this.sessionClient.signOut();
                      this.loginErrorMsg="Access Denied";
                    // this.redirection();
                     }
                }else{
                  //this.fetchEmployeeByUsername();
                  this.redirection();
                }



          }else{
          //  this.isLoginError=true;
            this.sessionClient.signOut();
            this.loginErrorMsg="Access Denied";
       //  this.redirection();
          }



      }else {
    //    this.isLoginError=true;
     //   this.sessionClient.signOut();
      //  this.loginErrorMsg="Access Denied";
     // this.fetchEmployeeByUsername();
       this.redirection();
     }


    },(err: HttpErrorResponse)=>{
      if (err.status === 200) {
        //json circular break at server side always add
            //extra information of timestamp and status along with
            //main json so parsing occurs still at status 200
            let jsonString=err.error.text;
            jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
            this.user=JSON.parse(jsonString);
            this.authService.setUserSession(this.user);

            if(this.user.accessControlMasterList!==null && this.user.accessControlMasterList!==undefined &&
              this.user.accessControlMasterList.length!==0){
                this.authService.setAccessControlData(this.user.accessControlMasterList);
                this.accessControlMasterMap=this.authService.getAccessControlMapData();

                if(this.accessControlMasterMap['LOGIN']!==null &&
                  this.accessControlMasterMap['LOGIN']!==undefined &&
                  this.accessControlMasterMap['LOGIN'].readAccess){



                  if(this.accessControlMasterMap['LOGIN'].staticIpAddress===null ||
                  this.accessControlMasterMap['LOGIN'].staticIpAddress===""){
                    this.redirection();
                  }else if(this.accessControlMasterMap['LOGIN'].staticIpAddress.includes(this.publicIpAddress) ){
                    console.log("ip matched public ");
                    this.redirection();
                  }


                }else{
                //  this.isLoginError=true;
                  this.sessionClient.signOut();
                  this.loginErrorMsg="Access Denied";
                }



            }else {
          //    this.isLoginError=true;
              this.loginErrorMsg="Access Denied";
           }

          //  this.redirection();
      }else if (err.status === 409) {
        this.loginErrorMsg=err.error;
        console.log("ERROR LOGIN "+err.error);
        this.sessionClient.signOut();
      }
      else{
        this.loginErrorMsg="Access Denied";
        console.log("ERROR LOGIN "+err.message);
      }

    }

    );
 }

 redirection(){
  this.router.navigateByUrl('/panel/okr/dashboard');

}
//network

getSystemProperties(){
  this.authService.systemProperties().subscribe(data => {
    this.authService.setProperties(data);
    this.properties=this.authService.getProperties();
    this.localIpAddress=this.authService.getLocalIpAddressData();
    this.publicIpAddress=this.authService.getPublicIpAddressData();
    this.fetchUserDetail();

   },(error: HttpErrorResponse)=>{
    console.log("error systemproperties");
   });

}

/*
public getPublicIpAddress(){
  this.authService.getIpAddress().subscribe(data => {
   // console.log(data);
   try{
    this.publicIpAddress=data.query;
    console.log("publicIpAddress "+this.publicIpAddress);
   }catch(err){

   }

  },(error: HttpErrorResponse)=>{
    console.error('Currently Offline: ', error);
  });
}


public getLocalIpAddress() {
  window.RTCPeerConnection = this.getRTCPeerConnection();

  const pc = new RTCPeerConnection({ iceServers: [] });
  pc.createDataChannel('');
  pc.createOffer().then(pc.setLocalDescription.bind(pc));

  pc.onicecandidate = (ice) => {
    this.zone.run(() => {
      if (!ice || !ice.candidate || !ice.candidate.candidate) {
        return;
      }

      this.localIpAddress = this.ipRegex.exec(ice.candidate.candidate)[1];
      sessionStorage.setItem('LOCAL_IP', this.localIpAddress);
      console.log("LOCAL_IP "+sessionStorage.getItem("LOCAL_IP"));
      pc.onicecandidate = () => {};
      pc.close();
    });
  };
}

private getRTCPeerConnection() {
  return window.RTCPeerConnection ||
    window.mozRTCPeerConnection ||
    window.webkitRTCPeerConnection;
}*/


getFieldConfigurationListByEntity(fieldConfiguration:FieldConfiguration){
  this.fieldConfigurationService.getFieldConfigurationListByEntity(fieldConfiguration).subscribe(data => {
    this.fieldConfigurationList=data;
   },(error: HttpErrorResponse)=>{
      if (error.status === 200) {
        //json circular break at server side always add
            //extra information of timestamp and status along with
            //main json so parsing occurs still at status 200
            let jsonString=error.error.text;
            jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
            this.fieldConfigurationList=JSON.parse(jsonString);

      }else{
        console.log("error getFieldConfigurationListByEntity");
      }
   });

}


getUserDetailsByUsername(){
  this.confirmUser = new UserLogin();
  if(this.forgotUser.username==null || this.forgotUser.username==undefined || this.forgotUser.username=="" ){
    return;
  }

  this.userService.getUserDetailsByUsername(this.forgotUser).subscribe( data => {
    
    if(data!=null && data!=undefined && data.id!=null && data.id!=0){
      this.confirmUser = data;
    }else{
      this.authService.addToast({title:'', msg:'User Doesnt Exist', showClose: true, 
      timeout: 5000, theme:'default', type:'error', position:'bottom-left', closeOther:true});
    }

  },error =>{
    if (error instanceof HttpErrorResponse) {

      if (error.status === 200) {
         // console.log(error.error.text);
         //json circular break at server side always add
         //extra information of timestamp and status along with
         //main json so parsing occurs still at status 200
         let jsonString=error.error.text;
         jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));

         if(jsonString!=null && jsonString!=undefined && jsonString.id!=null){
          this.confirmUser=JSON.parse(jsonString);
         // this.confirmUser = jsonString;
        }else{
          this.authService.addToast({title:'', msg:'User Doesnt Exist', showClose: true, 
          timeout: 5000, theme:'default', type:'error', position:'bottom-left', closeOther:true});
        }
       //  this.employeeMasterList=JSON.parse(jsonString);
      }else{
       // this.authService.showMessage('error','Something Went Wrong','');
    }

    }
  });
}

sendOtpToUser(){

  if(this.confirmUser.phone==null || this.confirmUser.phone==undefined || this.confirmUser.phone=="" ){
    return;
  }

  this.userService.sendOtpToUser(this.confirmUser).subscribe( data => {
    this.otp = data;
    this.forgotPassScreenIndex = 2;
  },error =>{
    if (error instanceof HttpErrorResponse) {

      if (error.status === 200) {
         // console.log(error.error.text);
         //json circular break at server side always add
         //extra information of timestamp and status along with
         //main json so parsing occurs still at status 200
    //     let jsonString=error.error.text;
    //     jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
      this.otp = error.error.text;
      this.forgotPassScreenIndex = 2;
       //  this.employeeMasterList=JSON.parse(jsonString);
      }else{
       // this.authService.showMessage('error','Something Went Wrong','');
    }

    }
  });
}


changePassword(){

  if(this.forgotUser.newPassword==null || this.forgotUser.newPassword==undefined || this.forgotUser.phone=="" ){
    return;
  }

  this.forgotUser.id = this.confirmUser.id;

  this.userService.forgetPassword(this.forgotUser).subscribe( data => {

    if(data=="SUCCESS"){
      this.authService.addToast({title:'', msg:'Password changed successfully', showClose: true, 
      timeout: 5000, theme:'default', type:'success', position:'bottom-left', closeOther:true});
      this.resetForm();
      this.forgotPasswordModalClose.hide();
    }else if(data=="NO-USER"){
      this.authService.addToast({title:'', msg:'User Doesnt Exist', showClose: true, 
      timeout: 5000, theme:'default', type:'error', position:'bottom-left', closeOther:true});
    }

   

    //this.userOtp = data;
  },error =>{
    if (error instanceof HttpErrorResponse) {

      if (error.status === 200) {
         // console.log(error.error.text);
         //json circular break at server side always add
         //extra information of timestamp and status along with
         //main json so parsing occurs still at status 200
    //     let jsonString=error.error.text;
    //     jsonString=jsonString.substr(0, jsonString.indexOf('{"timestamp"'));
      let data = error.error.text;

      if(data=="SUCCESS"){
        this.authService.addToast({title:'', msg:'Password changed successfully', showClose: true, 
        timeout: 5000, theme:'default', type:'success', position:'bottom-left', closeOther:true});
        this.resetForm();
      this.forgotPasswordModalClose.hide();
      }else if(data=="NO-USER"){
        this.authService.addToast({title:'', msg:'User Doesnt Exist', showClose: true, 
        timeout: 5000, theme:'default', type:'error', position:'bottom-left', closeOther:true});
      }
       //  this.employeeMasterList=JSON.parse(jsonString);
      }else{
       // this.authService.showMessage('error','Something Went Wrong','');
    }

    }
  });
}

resetForm(){
  this.forgotPassScreenIndex = 1;
  this.forgotUser = new UserLogin();
  this.userOtp = "";
  this.otp = "";
  this.confirmUser = new UserLogin();
}

forgotPasswordModal(){
  this.resetForm();
  this.forgotPasswordModalClose.show();
}



confirmOTP(){
  //let otp=localStorage.getItem("otp");

  if(this.otp!=null && this.otp!=undefined && this.otp!="" && this.userOtp!=null && this.userOtp!=undefined && this.userOtp!=""){
        if(this.otp==this.userOtp){
          this.forgotPassScreenIndex = 3;
          //this.authService.showMessage('success','Saved Successfully','');
        }else{
          this.authService.addToast({title:'', msg:'Wrong OTP!!!', showClose: true, 
          timeout: 5000, theme:'default', type:'error', position:'bottom-left', closeOther:true});
        }


  }else{
    //this.authService.showMessage('error','OTP is Mandatory!!!','');
    this.authService.addToast({title:'', msg:'OTP is Mandatory!!!', showClose: true, 
    timeout: 5000, theme:'default', type:'error', position:'bottom-left', closeOther:true});
  }

}

// closeForgotPasswordModal(){
//   this.forgotPasswordModalClose.nativeElement.click();
// }
}
