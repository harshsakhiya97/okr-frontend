export const environment = {
  production: true,
  apiUrl:"http://65.0.103.13:8080/OKRServer",
  okrDummyFilePath: "https://okr-s3-bucket.s3.ap-south-1.amazonaws.com/dummy/dummyfile.csv"
};
